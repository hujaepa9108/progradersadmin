<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Section_model extends CI_Model {

	public function countCourseUni($uniId)
    {
       $this->db->select("count(course.id) as total");  
	   $this->db->from("course_tab as course");
	   $this->db->join("university_tab as uni","uni.id=course.university_id");
	   $this->db->where("uni.id", $uniId);
	   return $this->db->get()->row();
    }

    public function countSection($courseId)
    {
       $this->db->select("count(section.id) as total");  
	   $this->db->from("section_tab as section");
	   $this->db->where("section.course_id", $courseId);
	   return $this->db->get()->row();
    }

    public function processAdd($data)
	{
		return $this->db->insert('section_tab', $data);
	}

    public function getUniName($uniId)
    {
       $this->db->select("university.name as name");  
	   $this->db->from("university_tab as university");
	   $this->db->where("university.id",$uniId);
	   $this->db->order_by("university.created_at", "DESC");
	   return $this->db->get()->row(); 
    }

    public function getCourseName($courseId)
    {
       $this->db->select("course.name as name,course.code as code");  
	   $this->db->from("course_tab as course");
	   $this->db->where("course.id",$courseId);
	   $this->db->order_by("course.created_at", "DESC");
	   return $this->db->get()->row(); 
    }

    ///////////////DATA TABLE STUFF <UNI>
	public function processUniListQuery()  
	{  
	   $this->db->select("university.name as name,university.id as id");  
	   $this->db->from("university_tab as university");
	   $this->db->order_by("university.created_at", "DESC");
	   if(isset($_POST["search"]["value"])) {  
	        $this->db->like("university.name", $_POST["search"]["value"]);
	   }

	}

	public function processUniListDB()
	{
		$this->processUniListQuery();  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	public function processUniListCount()  
      {  
           $this->processUniListQuery();
           return $this->db->count_all_results(); 
      }

    public function processUniListFiltered(){  
       $this->processUniListQuery();  
       $query = $this->db->get();  
       return $query->num_rows();  
    }
    //DATA TABLE STUFF <UNI>
    
    //DATA TABLE STUFF <COURSE>
	function courseListQuery($uniId)  
	  {   
	       $this->db->select("course.id as id, course.university_id as uni_id,course.pl_name as pl_name, course.name as name, course.code as code, course.description as description, course.created_at as created_at");  
	       $this->db->from("course_tab as course");
	       $this->db->where("course.university_id",$uniId);
	       if( isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
	            $this->db->like("course.name", $_POST["search"]["value"]);
	            $this->db->or_like("course.code",$_POST["search"]["value"]);
	       }  

	  }
	  public function courseListDB($uniId)
	{
		$this->courseListQuery($uniId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get(); 
           return $query->result(); 
	}

	function courseListCount($uniId)  
      {  
           $this->courseListQuery($uniId);
           return $this->db->count_all_results();  
      }

      function courseListFiltered($uniId){  
       $this->courseListQuery($uniId);  
       $query = $this->db->get();  
       return $query->num_rows();  
      }
	////////////////DATA TABLE STUFF <Course>
	///
	//DATA TABLE STUFF <section>
	function processListQuery($courseId)  
	  {   
	       $this->db->select("section.id as id, section.name as name, section.created_at as created_at");  
	       $this->db->from("section_tab as section");
	       // $this->db->join("section_user_tab as sut","sut.section_id=section.id");
	       // $this->db->join("user_tab as user","sut.user_id=user.id","left");
	       $this->db->where("section.course_id",$courseId);
	       if( isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
	            $this->db->like("section.name", $_POST["search"]["value"]);
	            // $this->db->or_like("instructor.email",$_POST["search"]["value"]);
	       }  

	  }

	public function processListDB($courseId)
	{
		$this->processListQuery($courseId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get(); 
           return $query->result(); 
	}

	function processListCount($courseId)  
      {  
           $this->processListQuery($courseId);
           return $this->db->count_all_results();  
      }

      function processListFiltered($courseId){  
       $this->processListQuery($courseId);  
       $query = $this->db->get();  
       return $query->num_rows();  
      }
      //Section data table stuff
      
      //count student in course
      public function countStudent($sectionId,$courseId)
      {
      	$this->db->select("count(student.id) as total");  
		$this->db->from("section_tab as section");
		$this->db->join("section_user_tab as sut","section.id=sut.section_id");
		$this->db->join("user_tab as student","sut.user_id=student.id");
		$this->db->where("sut.section_id",$sectionId);
		$this->db->where("section.course_id", $courseId);
		$this->db->where("student.role_id", 3);
		return $this->db->get()->row();
		  	
      }
      public function getInst($sectionId)
      {
      	$this->db->select("inst.name as name, inst.id as inst_id");  
		$this->db->from("section_tab as section");
		$this->db->join("section_user_tab as sut","section.id=sut.section_id");
		$this->db->join("user_tab as inst","sut.user_id=inst.id");
		$this->db->where("section.id", $sectionId);
		$this->db->where("inst.role_id", 2);
		return $this->db->get()->row();
      }

      public function getMembersInst($courseId,$instId="")
      {
      	$this->db->select("user.name as name, user.id as inst_id");  
		$this->db->from("user_tab as user");
		$this->db->join("course_user_tab as cut","user.id=cut.user_id");
		$this->db->join("course_tab as course","cut.course_id=course.id");
		if(!empty($instId))
			$this->db->where("user.id !=", $instId);
		$this->db->where("course.id", $courseId);
		$this->db->where("user.role_id", 2);
		return $this->db->get()->result();
      }

       public function getMembersStud($courseId,$sectionId)
      {
      	$this->db->select("user.name as name, user.email as email, student.student_metric_no as metric_no, user.created_at as created_at, user.id as user_id,sut.section_id as section_id");  
		$this->db->from("user_tab as user");
		$this->db->join("section_user_tab as sut","user.id=sut.user_id");
		$this->db->join("course_user_tab as cut","user.id=cut.user_id");
		$this->db->join("student_tab as student","user.id=student.student_id");
		$this->db->where("cut.course_id",$courseId);
		$this->db->where("sut.section_id",$sectionId);
		$this->db->where("user.role_id",3);
		$this->db->order_by("student.student_metric_no");
		return $this->db->get()->result();
      }

    public function assignInst($data)
	{	
		return $this->db->insert('section_user_tab', $data);
	}

	public function changeInst($instId,$sectionId,$data)
	{	

		return $this->db->simple_query("
			update section_user_tab as sut
			inner join user_tab as user on sut.user_id=user.id
			set sut.user_id=".$this->db->escape($instId)."
			where user.role_id=2
			and sut.section_id=".$sectionId."
		");
		// file_put_contents("debug.txt", print_r($query,true));
	}

	public function getNonSectStud($courseId,$sectionId)
    {
		$query = $this->db->query("select user.id as user_id, user.name as name, user.role_id as role_id,student.student_metric_no as metric_no from user_tab as user
		inner join course_user_tab as cut on user.id=cut.user_id
		inner join student_tab as student on user.id=student.student_id
		where user.id not in (select sut.user_id from section_user_tab as sut where sut.section_id=".$this->db->escape($sectionId).")
		and cut.course_id=".$this->db->escape($courseId));
		return $query->result();
    }
    public function addStud($sectionId,$studId)
    {
    	$this->db->insert('section_user_tab', array("section_id"=>$sectionId,"user_id"=>$studId));
    }

    public function removeStud($studentId,$sectionId) {
		return $this->db->delete('section_user_tab', array('user_id' => $studentId,"section_id"=>$sectionId)); 
	}
}

/* End of file section_model.php */
/* Location: ./application/models/section_model.php */