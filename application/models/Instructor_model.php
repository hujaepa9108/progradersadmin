<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instructor_model extends CI_Model {
	public function addInstDB($data,$courseId) {
		$this->db->insert('user_tab', $data);
		$instId=$this->db->insert_id();//get last inserted instructor id
		$data=array(
			"course_id" => $courseId,
			"user_id" => $instId,
		);
		$this->db->insert('course_user_tab', $data);
		$this->db->insert('first_time_login_tab', array("user_id"=>$instId));
		return true;
	}

	public function countInstUni($uniId)
    {
       $this->db->select("count(user.id) as total");  
	   $this->db->from("user_tab as user");
	   $this->db->join("university_tab as uni","uni.id=user.uni_id");
	   $this->db->where("user.role_id", 2);
	   $this->db->where("uni.id", $uniId);
	   return $this->db->get()->row();
    }

    public function getTotalSections($instId)
    {
       $this->db->select("count(sut.user_id) as total");  
	   $this->db->from("section_user_tab as sut");
	   $this->db->join("user_tab as user","sut.user_id=user.id");
	   $this->db->where("user.id", $instId);
	   $this->db->where("user.role_id", 2);
	   return $this->db->get()->row();
    }

    public function getUniName($uniId)
    {
       $this->db->select("university.name as name");  
	   $this->db->from("university_tab as university");
	   $this->db->where("university.id",$uniId);
	   $this->db->order_by("university.created_at", "DESC");
	   return $this->db->get()->row(); 
    }

	// public function getTotalCourse()
	// {
	// 	$this->db->select("count(ct_id) as total");
	// 	$this->db->from("course_tab");
	// 	$query = $this->db->get()->row(); 
	// 	return $query;
	// }

	// public function getTotalSection()
	// {
	// 	$this->db->select("count(section_id) as total");
	// 	$this->db->from("section_tab");
	// 	$query = $this->db->get()->row(); 
	// 	return $query;
	// }

	public function checkExistDB($email){
		$this->db->select("email");
		$this->db->from("user_tab");
		$this->db->where("email",$email);
		$query=$this->db->get();
		return $query->num_rows();
	}

	//DATA TABLE STUFF
	function processListQuery($uniId)  
	  {   
	       $this->db->select("instructor.id as id,instructor.uni_id as uni_id, instructor.name as name, instructor.email as email, instructor.created_at as created_at");  
	       $this->db->from("user_tab as instructor");
	       $this->db->where("instructor.uni_id",$uniId);
	       $this->db->where("instructor.role_id",2);
	       if( isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
	            // $this->db->like("instructor.name", $_POST["search"]["value"]);
	            $this->db->like("instructor.email",$_POST["search"]["value"]);
	       }  

	  }

	public function processListDB($uniId)
	{
		$this->processListQuery($uniId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get(); 
        //    file_put_contents("debug.txt", $this->db->Last_query()); 
           return $query->result(); 
	}

	function processListCount($uniId)  
      {  
           $this->processListQuery($uniId);
           return $this->db->count_all_results();  
      }

      function processListFiltered($uniId){  
       $this->processListQuery($uniId);  
       $query = $this->db->get();  
       return $query->num_rows();  
      }

    ///////////////DATA TABLE STUFF <UNI>
	public function processUniListQuery()  
	{  
	   $this->db->select("university.name as name,university.id as id");  
	   $this->db->from("university_tab as university");
	   $this->db->order_by("university.created_at", "DESC");
	   if(isset($_POST["search"]["value"])) {  
	        $this->db->like("university.name", $_POST["search"]["value"]);
	   }

	}

	public function processUniListDB()
	{
		$this->processUniListQuery();  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	public function processUniListCount()  
      {  
           $this->processUniListQuery();
           return $this->db->count_all_results(); 
      }

    public function processUniListFiltered(){  
       $this->processUniListQuery();  
       $query = $this->db->get();  
       return $query->num_rows();  
    }
    //DATA TABLE STUFF <UNI>
      
      public function deleteDB($userid) {
		return $this->db->delete('user_tab', array('id' => $userid)); 
	}

	public function getCourseDB($uniId) {
		$this->db->select("name,id,code");
		$this->db->from("course_tab");
		$this->db->where("university_id",$uniId);
		$this->db->order_by("code",'ASC');
		return $this->db->get()->result();
	}

	public function countCourseUni($uniId)
    {
       $this->db->select("course.id as id");  
	   $this->db->from("course_tab as course");
	   $this->db->join("university_tab as uni","uni.id=course.university_id");
	   $this->db->where("uni.id", $uniId);
	   return $this->db->get()->num_rows();
    }
}

/* End of file Instructor_model.php */
/* Location: ./application/models/Instructor_model.php */