<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function totalUni()
	{
		$this->db->select("count(id) as total");
		$this->db->from("university_tab");
		return $this->db->get()->row();
	}

	public function totalCourse()
	{
		$this->db->select("count(id) as total");
		$this->db->from("course_tab");
		return $this->db->get()->row();
	}

	public function totalInst()
	{
		$this->db->select("count(id) as total");
		$this->db->from("user_tab");
		$this->db->where("role_id",2);
		return $this->db->get()->row();
	}

	public function totalStudent()
	{
		$this->db->select("count(id) as total");
		$this->db->from("user_tab");
		$this->db->where("role_id",3);
		return $this->db->get()->row();
	}

	public function totalAssignment()
	{
		$this->db->select("count(id) as total");
		$this->db->from("assignment_tab");
		return $this->db->get()->row();
	}

	//DATA TABLE STUFF
	public function processListQuery()  
	  {  
	  	   $this->db->select("user.name as name, role.name as role_name, logged.start_login as login, logged.end_login as logout");
	  	   $this->db->from("user_tab as user");
	  	   $this->db->join("role_tab as role", "user.role_id=role.id");
	  	   $this->db->join("logged_user_tab as logged", "user.id=logged.user_id");
	       $this->db->order_by("logged.start_login", "DESC");
	       if(isset($_POST["search"]["value"])&& !empty(($_POST["search"]["value"])) ) {  
	            $this->db->like("user.name", $_POST["search"]["value"]);
	       }

	  }

	public function processListDB()
	{
		$this->processListQuery();  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	function processListCount()  
      {  
           $this->processListQuery();
           return $this->db->count_all_results();  
      }

      function processListFiltered(){  
       $this->processListQuery();  
       $query = $this->db->get();  
       return $query->num_rows();  
      }
      //DATA TABLE STUFF
}

/* End of file dashboard_model.php */
/* Location: ./application/models/dashboard_model.php */