<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends CI_Model {
	public function addStudentDB($data,$courseId,$idno) {
		$this->db->insert('user_tab', $data);
		$studId=$this->db->insert_id();//get last inserted instructor id
		$data=array(
			"course_id" => $courseId,
			"user_id" => $studId,
		);

		$this->db->insert('course_user_tab', $data);
		$data=array(
			"student_metric_no" => $idno,
			"student_id" => $studId,
		);
		$this->db->insert('student_tab', $data);
		$this->db->insert('first_time_login_tab', array("user_id"=>$studId));
		return true;
	}

	public function countStudentUni($uniId)
    {
       $this->db->select("count(user.id) as total");  
	   $this->db->from("user_tab as user");
	   $this->db->join("university_tab as uni","uni.id=user.uni_id");
	   $this->db->where("user.role_id", 3);
	   $this->db->where("uni.id", $uniId);
	   return $this->db->get()->row();
    }

    public function getUniName($uniId)
    {
       $this->db->select("university.name as name");  
	   $this->db->from("university_tab as university");
	   $this->db->where("university.id",$uniId);
	   $this->db->order_by("university.created_at", "DESC");
	   return $this->db->get()->row(); 
    }

	public function checkExistDB($email){
		$this->db->select("email");
		$this->db->from("user_tab");
		$this->db->where("email",$email);
		$this->db->where("role_id",3);
		$query=$this->db->get();
		return $query->num_rows();
	}

	//DATA TABLE STUFF
	function processListQuery($uniId)  
	  {   
	       $this->db->select("student.id as id,student.uni_id as uni_id, student.name as name, student.email as email, student.created_at as created_at, sut.student_metric_no as idno, course.code as code");  
	       $this->db->from("user_tab as student");
		   $this->db->join("student_tab as sut","sut.student_id=student.id");
		   $this->db->join("course_user_tab as cut","cut.user_id=student.id");
		   $this->db->join("course_tab as course","course.id=cut.course_id");
	       $this->db->where("student.uni_id",$uniId);
	       $this->db->where("student.role_id",3);
	       if( isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
	            $this->db->like("student.name", $_POST["search"]["value"]);
	            $this->db->or_like("student.email",$_POST["search"]["value"]);
	            $this->db->or_like("sut.student_metric_no",$_POST["search"]["value"]);
	       }  
	       $this->db->order_by("student.created_at,course.code","DESC");
	  }

	public function processListDB($uniId)
	{
		$this->processListQuery($uniId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get(); 
           return $query->result(); 
	}

	function processListCount($uniId)  
      {  
           $this->processListQuery($uniId);
           return $this->db->count_all_results();  
      }

      function processListFiltered($uniId){  
       $this->processListQuery($uniId);  
       $query = $this->db->get();  
       return $query->num_rows();  
      }

    ///////////////DATA TABLE STUFF <UNI>
	public function processUniListQuery()  
	{  
	   $this->db->select("university.name as name,university.id as id");  
	   $this->db->from("university_tab as university");
	   $this->db->order_by("university.created_at", "DESC");
	   if(isset($_POST["search"]["value"])) {  
	        $this->db->like("university.name", $_POST["search"]["value"]);
	   }

	}

	public function processUniListDB()
	{
		$this->processUniListQuery();  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	public function processUniListCount()  
      {  
           $this->processUniListQuery();
           return $this->db->count_all_results(); 
      }

    public function processUniListFiltered(){  
       $this->processUniListQuery();  
       $query = $this->db->get();  
       return $query->num_rows();  
    }
    //DATA TABLE STUFF <UNI>
      
      public function deleteDB($userid) {
		return $this->db->delete('user_tab', array('id' => $userid)); 
	}

	public function getCourseDB() {
		$this->db->select("name,id,code");
		$this->db->from("course_tab");
		$this->db->order_by("code",'ASC');
		return $this->db->get()->result();
	}

	public function countCourseUni($uniId)
    {
       $this->db->select("course.id as id");  
	   $this->db->from("course_tab as course");
	   $this->db->join("university_tab as uni","uni.id=course.university_id");
	   $this->db->where("uni.id", $uniId);
	   return $this->db->get()->num_rows();
    }
}

/* End of file Instructor_model.php */
/* Location: ./application/models/Instructor_model.php */