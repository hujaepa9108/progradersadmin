<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {
	//processing user login
	public function loginDB($username) {
		$this->db->select("username,password,id");
		$this->db->from("admin_tab");
		$this->db->where("username",$username);
		$query = $this->db->get()->row();
		return $query;
	}
}