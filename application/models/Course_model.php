<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course_model extends CI_Model {

	public function deleteDB($courseid) {
		return $this->db->delete('course_tab', array('id' => $courseid)); 
	}

	/*
	This function is to retrieve course data for adding instructor form. It will populate the dropdown for adding form
	 */
	public function getCourseDB() {
		$this->db->select("name,id,code");
		$this->db->from("course_tab");
		$this->db->order_by("name",'ASC');
		return $this->db->get()->result();
	}
	/*
	Processing adding course
	 */
	public function addCourseDB($data)	{
		return $this->db->insert('course_tab', $data);
	}
	/*
	check if course exist. mainly used for validation
	 */
	public function checkExistDB($course){

		$this->db->select("code");
		$this->db->from("course_tab");
		$this->db->where("code",$course);
		$query=$this->db->get();
		return $query->num_rows();
	}

	public function checkExistEdit($code,$courseId){

		$this->db->select("code");
		$this->db->from("course_tab");
		$this->db->where("code",$code);
		$this->db->where("id !=",$courseId);
		$query=$this->db->get();
		return $query->num_rows();
	}

	//DATA TABLE STUFF
	function processListQuery($uniId)  
	  {   
	       $this->db->select("course.id as id, course.university_id as uni_id,course.pl_name as pl_name, course.name as name, course.code as code, course.description as description, course.created_at as created_at");  
	       $this->db->from("course_tab as course");
	       $this->db->where("course.university_id",$uniId);
	       if( isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
	            $this->db->like("course.name", $_POST["search"]["value"]);
	            $this->db->or_like("course.code",$_POST["search"]["value"]);
	       }  

	  }

	public function processListDB($uniId)
	{
		$this->processListQuery($uniId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	function processListCount($uniId)  
      {  
           $this->processListQuery($uniId);
           return $this->db->count_all_results();  
      }

      function processListFiltered($uniId){  
       $this->processListQuery($uniId);  
       $query = $this->db->get();  
       return $query->num_rows();  
      }
	////////////////DATA TABLE STUFF <Course>
      
	///////////////DATA TABLE STUFF <UNI>
	public function processUniListQuery()  
	{  
	   $this->db->select("university.name as name,university.id as id");  
	   $this->db->from("university_tab as university");
	   $this->db->order_by("university.created_at", "DESC");
	   if(isset($_POST["search"]["value"])) {  
	        $this->db->like("university.name", $_POST["search"]["value"]);
	   }

	}

	public function processUniListDB()
	{
		$this->processUniListQuery();  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	public function processUniListCount()  
      {  
           $this->processUniListQuery();
           return $this->db->count_all_results(); 
      }

    public function processUniListFiltered(){  
       $this->processUniListQuery();  
       $query = $this->db->get();  
       return $query->num_rows();  
    }
    //DATA TABLE STUFF <UNI>
    
    public function getUniName($uniId)
    {
       $this->db->select("university.name as name");  
	   $this->db->from("university_tab as university");
	   $this->db->where("university.id",$uniId);
	   $this->db->order_by("university.created_at", "DESC");
	   return $this->db->get()->row(); 
    }

    public function getCourseInfo($courseId)
    {
    	$this->db->select("university_id,name,code,uni_level,pl_id,pl_name,description");
    	$this->db->from("course_tab");
		$this->db->where("id",$courseId);
		$query=$this->db->get();
		return $query->row();
    }
    
    public function countCourseUni($uniId)
    {
       $this->db->select("count(course.id) as total");  
	   $this->db->from("university_tab as university");
	   $this->db->join("course_tab as course","course.university_id=university.id");
	   $this->db->where("university.id", $uniId);
	   $this->db->order_by("university.created_at", "DESC");
	   return $this->db->get()->row();
    }

    public function processEdit($data,$courseId)
    {
    	return $this->db->update("course_tab",$data,"id=".base64_decode($courseId));
    }
}

/* End of file modelName.php */
/* Location: ./application/models/modelName.php */