<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class University_model extends CI_Model {

	public function ajaxUniExist($uniName){

		$this->db->select("id");
		$this->db->from("university_tab");
		$this->db->where("name",$uniName);
		$query=$this->db->get();
		return $query->num_rows();
	}
	public function processAdd($data)
	{
		return $this->db->insert('university_tab', $data);
	}

	//DATA TABLE STUFF
	public function processListQuery()  
	  {  
	       $this->db->select("university.name as name,university.created_at as created_at, university.id as id");  
	       $this->db->from("university_tab as university");
	       $this->db->order_by("university.created_at", "DESC");
	       if(isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
	            $this->db->like("university.name", $_POST["search"]["value"]);
	       }

	  }

	public function processListDB()
	{
		$this->processListQuery();  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	public function processListCount()  
      {  
           $this->processListQuery();
           return $this->db->count_all_results();  
      }

    public function processListFiltered(){  
       $this->processListQuery();  
       $query = $this->db->get();  
       return $query->num_rows();  
    }
    //DATA TABLE STUFF
    
    public function getUniName($uniId)
    {
    	$this->db->select("name");
    	$this->db->from("university_tab");
		$this->db->where("id",base64_decode($uniId));
		$query=$this->db->get();
		return $query->row();
    }

    public function processEdit($data,$id)
    {
    	return $this->db->update("university_tab",$data,"id=".base64_decode($id));
    }

    public function processDelete($id) {
		return $this->db->delete('university_tab', array('id' => base64_decode($id))); 
	}
}

/* End of file modelName.php */
/* Location: ./application/models/modelName.php */