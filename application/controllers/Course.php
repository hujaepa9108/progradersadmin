<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends My_Controller {

	//COURSE STUFF//
	public function __construct() {
		parent::__construct();
		$this->load->model('Course_model','course_m');
		$this->load->library('form_validation');
		if(!$this->auth()) 
			redirect("login");
	}

	public function add($uniId,$uniName) {
		$data['title']="Add New Course";
		// $url=explode("/", $_SERVER['REQUEST_URI']);
		// $length=count($url);
		$data['uniId']=$uniId;
		// $data['uni']=$this->course_m->getUniName(base64_decode($this->uri->segment($length-2)));
		$data['uniName']=base64_decode($uniName);
		echo $this->blade->view()->make("course.add",$data);
	}

	public function uni($type='') {
		$data['title']="List of university";
		$data["type"]=$type;
		echo $this->blade->view()->make("course.uni",$data);
	}

	//DATA TABLE STUFF <Course>
	public function processList($uniId)
	{
		//fetch data
	   $str_uniId=base64_decode($uniId);
	   $fetch_data = $this->course_m->processListDB($str_uniId);  
       $data = array();
       foreach($fetch_data as $row)  
       {  
           	//SETTING THE BUTTONS FOR EDIT AND DELETE. FOR READABILITY

			$butt_edit = "<a data-toggle='tooltip' title='Edit course info' href='".base_url()."course/edit/".base64_encode($row->uni_id)."/".base64_encode($row->id)."' name='update' class='btn btn-warning btn-sm'><i class='fas fa-pencil-alt'></i></a>";

			$butt_delete = "<button type='button' data-toggle='tooltip' title='Delete course info' name='update' onclick='remove(\"".base64_encode($row->id)."\",\"".$row->name."\")' class='btn btn-danger btn-sm'><i class='fas fa-trash-alt'></i></button>";

	        $sub_array = array();
	        $sub_array[] ="";   
	        $sub_array[] = $row->name;
	        $sub_array[] = $row->code;
	        $sub_array[] = $row->pl_name;
	        $sub_array[] = $row->created_at;
	        $sub_array[] = $butt_edit."&nbsp;&nbsp;".$butt_delete;
	        $data[] = $sub_array;  
        }  
       $output = array(  
            "draw" => intval($_POST["draw"]),  
            "recordsTotal" => $this->course_m->processListCount($str_uniId),  
            "recordsFiltered" => $this->course_m->processListFiltered($str_uniId),  
            "data" => $data  
       );  
       echo json_encode($output);
	}


	public function processAdd()
	{
		if(!empty($this->input->post())){
			$this->form_validation->set_rules('code', 'code', 'required');
			$this->form_validation->set_rules('name', 'name', 'required');
			$this->form_validation->set_rules('description', 'description', 'required');
			$this->form_validation->set_rules('level', 'level', 'required');
			$this->form_validation->set_rules('language', 'language', 'required');
			
			// form validation
			if(!$this->form_validation->run())
				echo $this->blade->view()->make("course.add");

			else {
				extract($this->input->post());//extract input
				$status = $this->initiateStat();//intiate status

				$data=array(
					"code"=> strtoupper($code),
					"name"=> strtoupper($name),
					"description"=> $description,
					"university_id"=>base64_decode($uniId),
					"pl_id" => $language,
					"pl_name"=>$pl_name_hidden,
					"uni_level" => $level,
					"created_at"=> date('Y-m-d H:i:s'),
					"created_by" => $this->decrypt($_SESSION['user_id'])

				);

				if($this->course_m->addCourseDB($data)){
					$status['val']=true;
					$status['msg']="Successfully added new course";
					echo json_encode($status);
				}

				else {
					$status['val']=false;
					$status['msg']="Technical Error!";
					echo json_encode($status);
				}
			}//end of else form validation
		}//end of isset post
	}

	public function getCourse()	{
		$data['course']=$this->course_m->getCourseDB();
		echo json_encode($data);
	}

	public function checkExist()
	{	
		if($this->course_m->checkExistDB($this->input->get('code'),$this->input->get('courseId'))>0)
			echo "false";
		else
			echo "true";
	}

	public function checkExistEdit($courseId)
	{	
		if($this->course_m->checkExistEdit($this->input->get('code'),base64_decode($courseId))>0)
			echo "false";
		else
			echo "true";
	}

	public function list($uniId,$uniName)
	{
		$data['title']="List of Course";
		// $url=explode("/", $_SERVER['REQUEST_URI']);
		// $length=count($url);
		$data['uniId']=$uniId;
		// $data['uni']=$this->course_m->getUniName(base64_decode($this->uri->segment($length-2)));
		// $data['uni']=$this->course_m->getUniName(base64_decode();
		$data["uniName"]=base64_decode($uniName);
		// echo $uniName;
		echo $this->blade->view()->make("course.list",$data);
	}
	
	//DATA TABLE STUFF <UNI>
	public function uniList($type)
	{
	   //fetch data
	   $fetch_data = $this->course_m->processUniListDB();  
       $data = array();
       foreach($fetch_data as $row)  
       {  
       		//SETTING THE BUTTONS FOR EDIT AND DELETE. FOR READABILITY
        	//COUNT COURSE
        	$course=$this->course_m->countCourseUni($row->id);
	        if($type!="list"){
				$butt_view = "<a href='".base_url()."course/add/".base64_encode($row->id)."/".base64_encode($row->name)."' data-toggle='tooltip' title='Add New Course' class='btn btn-success btn-sm'><i class='fas fa-plus-circle'></i></a>";
	        }

	        else {
	        	$butt_view = "<a href='".base_url()."course/list/".base64_encode($row->id)."/".base64_encode($row->name)."' data-toggle='tooltip' title='View Course' class='btn btn-info btn-sm'><i class='fas fa-list-alt'></i></a>";
	        }

            $sub_array = array();
            $sub_array[] ="";   
            $sub_array[] = $row->name;
            $sub_array[] = $course->total;
            $sub_array[] = $butt_view;
            $data[] = $sub_array;  
        }  
       $output = array(  
            "draw" => intval($_POST["draw"]),  
            "recordsTotal" => $this->course_m->processUniListCount(),  
            "recordsFiltered" => $this->course_m->processUniListFiltered(),  
            "data" => $data  
       );  
       echo json_encode($output);
	}

	public function edit($uniId,$courseId)
	{
		$data["title"]="Edit Course Info";
		$data["courseId"] =$courseId;
		$data["course"] = $this->course_m->getCourseInfo(base64_decode($courseId));
		echo $this->blade->view()->make("course.edit",$data);
	}

	public function processEdit()
	{
		if(!empty($this->input->post())) {
			// form validation
			$this->form_validation->set_rules('code', 'code', 'required');
			$this->form_validation->set_rules('name', 'name', 'required');
			$this->form_validation->set_rules('description', 'description', 'required');

			if(!$this->form_validation->run())
				echo $this->blade->view()->make("course.edit");

			else {
				extract($this->input->post());//extract input
				$status = $this->initiateStat();//intiate status

				$data=array(
					"code"=> strtoupper($code),
					"name"=> strtoupper($name),
					"description"=> $description,
					"uni_level"=> strtoupper($level),
					"pl_id"=> $language_id,
					"pl_name"=> $language_name
				);

				if($this->course_m->processEdit($data,$courseId)){
					$status['val']=true;
					$status['msg']="Successfully edited the info";
					echo json_encode($status);
				}

				else {
					$status['val']=false;
					$status['msg']="Technical Error!";
					echo json_encode($status);
				}
			}//end of else form validation
		}//end of isset post
	}

	public function processDelete($courseid)
	{
		$status = $this->initiateStat();//intiate status
		if($this->course_m->deleteDB(base64_decode($courseid))) {
			$status['val']=true;
			$status['msg']="success";
		}

		else {
			$status['val']=false;
			$status['msg']="fail!";
		}
		echo json_encode($status);
	}
}