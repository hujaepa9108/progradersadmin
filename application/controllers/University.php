<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class University extends My_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("University_model", "university_m");
		if(!$this->auth()) 
			redirect("login");
	}

	public function add()
	{
		$data['title']="Add new university";
		echo $this->blade->view()->make("university.add",$data);
	}

	public function processAdd()
	{
		if(!empty($this->input->post())) {
			// form validation
			$this->form_validation->set_rules('university', 'university', 'required');
			if(!$this->form_validation->run())
				echo $this->blade->view()->make("university.add");

			else {
				extract($this->input->post());//extract input
				$status = $this->initiateStat();//intiate status

				$data=array(
					"name"=> strtoupper($university),
					"created_at"=> date('Y-m-d H:i:s'),
					"created_by"=> $this->decrypt($_SESSION["user_id"])
				);

				if($this->university_m->processAdd($data)){
					$status['val']=true;
					$status['msg']="Successfully added new university";
					echo json_encode($status);
				}

				else {
					$status['val']=false;
					$status['msg']="Technical Error!";
					echo json_encode($status);
				}
			}//end of else form validation
		}//end of isset post
	}

	public function ajaxUniExist()
	{
		if($this->university_m->ajaxUniExist($this->input->get('university')) > 0)
			echo "false";
		else
			echo "true";
	}

	public function list()
	{
		$data['title']="University List";
		echo $this->blade->view()->make("university.list",$data);
	}

	//DATA TABLE STUFF
	public function processList()
	{
		//fetch data
		$fetch_data = $this->university_m->processListDB();  
           $data = array();
           foreach($fetch_data as $row)  
           {  
           	//SETTING THE BUTTONS FOR EDIT AND DELETE. FOR READABILITY
		$butt_edit = "<a href='".base_url()."university/edit/".base64_encode($row->id)."' data-toggle='tooltip' title='Edit University Info' name='update' class='btn btn-warning btn-sm'><i class='fas fa-pencil-alt'></i></a>";

		$butt_delete = "<button type='button' data-toggle='tooltip' title='Delete University' name='update' onclick='remove(\"".base64_encode($row->id)."\",\"".$row->name."\")' class='btn btn-danger btn-sm'><i class='fas fa-trash-alt'></i></button>";

                $sub_array = array();
                $sub_array[] ="";   
                $sub_array[] = $row->name;  
                $sub_array[] = "<i class='fas fa-clock'></i>&nbsp;".date("h:i:s A",strtotime($row->created_at))."&nbsp;&nbsp;<i class='fas fa-calendar-alt'></i>&nbsp;".date("d-M-Y",strtotime($row->created_at));
                $sub_array[] = $butt_edit."&nbsp;&nbsp;".$butt_delete;
                $data[] = $sub_array;  
           }  
           $output = array(  
                "draw" => intval($_POST["draw"]),  
                "recordsTotal" => $this->university_m->processListCount(),  
                "recordsFiltered" => $this->university_m->processListFiltered(),  
                "data" => $data  
           );  
           echo json_encode($output);
	}



	public function edit($uniId)
	{
		$data["title"]="Edit University Name";
		$data["id"] =$uniId;
		$data["uni"] = $this->university_m->getUniName($uniId);
		echo $this->blade->view()->make("university.edit",$data);
	}

	public function processEdit()
	{
		if(!empty($this->input->post())) {
			// form validation
			$this->form_validation->set_rules('university', 'university', 'required');
			if(!$this->form_validation->run())
				echo $this->blade->view()->make("university.edit");

			else {
				extract($this->input->post());//extract input
				$status = $this->initiateStat();//intiate status

				$data=array(
					"name"=> strtoupper($university)
				);

				if($this->university_m->processEdit($data,$id)){
					$status['val']=true;
					$status['msg']="Successfully edited the info";
					echo json_encode($status);
				}

				else {
					$status['val']=false;
					$status['msg']="Technical Error!";
					echo json_encode($status);
				}
			}//end of else form validation
		}//end of isset post
	}

	public function processDelete($id)
	{
		$status = $this->initiateStat();//intiate status
		if($this->university_m->processDelete($id)) {
			$status['val']=true;
			$status['msg']="success";
		}

		else {
			$status['val']=false;
			$status['msg']="fail!";
		}
		echo json_encode($status);
	}
}

/* End of file university.php */
/* Location: ./application/controllers/university.php */