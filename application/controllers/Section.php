<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Section extends My_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Section_model','section_m');
		$this->load->library('form_validation');
		if(!$this->auth()) 
			redirect("login");
	}

	public function uni() {
		$data['title']="List of university";
		echo $this->blade->view()->make("section.uni",$data);
	}

	public function course($uniId) {
		$data['title']="List of university";
		$data['uni']=$this->section_m->getUniName(base64_decode($uniId));
		echo $this->blade->view()->make("section.course",$data);
	}

	public function add($uniId,$courseId)
	{
		$data['uni']=$this->section_m->getUniName(base64_decode($uniId));
		$data['course']=$this->section_m->getCourseName(base64_decode($courseId));
		$data['uniId']=$uniId;
		$data['courseId']=$courseId;
		echo $this->blade->view()->make("section.add",$data);
	}


	public function processAdd()
	{
		if(!empty($this->input->post())) {
			// form validation
			$this->form_validation->set_rules('section', 'section', 'required');
			if(!$this->form_validation->run())
				echo $this->blade->view()->make("section.add");

			else {
				extract($this->input->post());//extract input
				$status = $this->initiateStat();//intiate status

				$data=array(
					"name"=> strtoupper($section),
					"course_id" => base64_decode($courseId),
					"created_at"=> date('Y-m-d H:i:s')
				);

				if($this->section_m->processAdd($data)){
					$status['val']=true;
					$status['msg']="Successfully created new section";
					echo json_encode($status);
				}

				else {
					$status['val']=false;
					$status['msg']="Technical Error!";
					echo json_encode($status);
				}
			}//end of else form validation
		}//end of isset post
	}

	//DATA TABLE STUFF <UNI>
	public function uniList($type)
	{
	   //fetch data
	   $fetch_data = $this->section_m->processUniListDB();  
       $data = array();
       foreach($fetch_data as $row)  
       {  
       		//SETTING THE BUTTONS FOR EDIT AND DELETE. FOR READABILITY
        	
        	$course=$this->section_m->countCourseUni($row->id);//COUNT COURSE
        	$butt_view = "<a href='".base_url()."section/course/".base64_encode($row->id)."' data-toggle='tooltip' title='View Courses' class='btn btn-info btn-sm'><i class='fas fa-list-alt'></i></a>";

            $sub_array = array();
            $sub_array[] ="";   
            $sub_array[] = $row->name;
            $sub_array[] = $course->total;
            $sub_array[] = $butt_view;
            $data[] = $sub_array;  
        }  
       $output = array(  
            "draw" => intval($_POST["draw"]),  
            "recordsTotal" => $this->section_m->processUniListCount(),  
            "recordsFiltered" => $this->section_m->processUniListFiltered(),  
            "data" => $data  
       );  
       echo json_encode($output);
	}

	//DATA TABLE STUFF <Course>
	public function courseList($uniId)
	{
		//fetch data
	   $str_uniId=base64_decode($uniId);
	   $fetch_data = $this->section_m->courseListDB($str_uniId);  
       $data = array();
       foreach($fetch_data as $row)  
       {  
           	//SETTING THE BUTTONS FOR EDIT AND DELETE. FOR READABILITY
       		$section=$this->section_m->countSection($row->id);//COUNT SECTION

			$butt_view = "<a data-toggle='tooltip' title='View Sections' href='".base_url()."section/list/".base64_encode($row->uni_id)."/".base64_encode($row->id)."' name='view' class='btn btn-info btn-sm'><i class='fas fa-list-alt'></i></a>";

			$butt_add = "<a data-toggle='tooltip' title='Add New Sections' href='".base_url()."section/add/".base64_encode($row->uni_id)."/".base64_encode($row->id)."' name='view' class='btn btn-success btn-sm'><i class='fas fa-plus'></i></a>";

	        $sub_array = array();
	        $sub_array[] ="";   
	        $sub_array[] = $row->name;
	        $sub_array[] = $row->code;
	        $sub_array[] = $row->pl_name;
	        $sub_array[] = $section->total;
	        $sub_array[] = $row->created_at;
	        $sub_array[] = $butt_view." ".$butt_add;
	        $data[] = $sub_array;  
        }  
       $output = array(  
            "draw" => intval($_POST["draw"]),  
            "recordsTotal" => $this->section_m->courseListCount($str_uniId),  
            "recordsFiltered" => $this->section_m->courseListFiltered($str_uniId),  
            "data" => $data  
       );  
       echo json_encode($output);
	}

	// public function getCourse()	{
	// 	$data['course']=$this->section_m->getCourseDB();
	// 	echo json_encode($data);
	// }
	public function members($uniId,$courseId,$sectionId)
	{
		$data['uniId']=$uniId;
		$data['uni']=$this->section_m->getUniName(base64_decode($uniId));
		$data['courseId']=$courseId;
		$data['course']=$this->section_m->getCourseName(base64_decode($courseId));
		$data['sectionId']=$sectionId;
		$data['inst']=$this->section_m->getInst(base64_decode($sectionId));
		if(!empty($data['inst']))
			$data['instId']=base64_encode($data['inst']->inst_id);
		echo $this->blade->view()->make("section.members",$data);
	}
	public function list($uniId,$courseId)
	{
		$data['uniId']=$uniId;
		$data['courseId']=$courseId;
		$data['uni']=$this->section_m->getUniName(base64_decode($uniId));
		$data['course']=$this->section_m->getCourseName(base64_decode($courseId));
		echo $this->blade->view()->make("section.list",$data);
	}

	//DATA TABLE STUFF <Course>
	public function processList($uniId,$courseId)
	{
		//fetch data
	   $str_courseId=base64_decode($courseId);
	   $fetch_data = $this->section_m->processListDB($str_courseId);  
       $data = array();
       foreach($fetch_data as $row)  
       {  

       		//fetch assigned instrcutor
       		$instructor=$this->section_m->getInst($row->id);
       		$assign_inst= (!isset($instructor)) ? "<i><b class='text-danger'>None</b></i>" : $instructor->name;

       		//count total studnet in section
       		$student=$this->section_m->countStudent($row->id,$str_courseId);

           	//SETTING THE BUTTONS FOR EDIT AND DELETE. FOR READABILITY
       		// $student = $this->section_m->countStudent($str_courseId);
			$butt_edit = "<a data-toggle='tooltip' title='Edit section label' href='".base_url()."section/edit/".base64_encode($row->id)."' name='update' class='btn btn-warning btn-sm'><i class='fas fa-pencil-alt'></i></a>";

			$butt_view = "<a href='".base_url()."section/members/".$uniId."/".$courseId."/".base64_encode($row->id)."' data-toggle='tooltip' title='View Instructor/Students' class='btn btn-info btn-sm'><i class='fas fa-list-alt'></i></a>";

			// $butt_delete = "<button type='button' data-toggle='tooltip' title='Delete course info' name='update' onclick='remove(\"".base64_encode($row->id)."\",\"".$row->name."\")' class='btn btn-danger btn-sm'><i class='fas fa-trash-alt'></i></button>";

	        $sub_array = array();
	        $sub_array[] ="";   
	        $sub_array[] = $row->name;
	        $sub_array[] = "<b><i class='text-primary'>".$assign_inst."</i></b>";
	        $sub_array[] = $student->total;
	        $sub_array[] = $row->created_at;
	        $sub_array[] = $butt_view."&nbsp;".$butt_edit;
	        $data[] = $sub_array;  
        }  
       $output = array(  
            "draw" => intval($_POST["draw"]),  
            "recordsTotal" => $this->section_m->processListCount($str_courseId),  
            "recordsFiltered" => $this->section_m->processListFiltered($str_courseId),  
            "data" => $data  
       );  
       echo json_encode($output);
	}

	public function getMembersInst($courseId,$instId='')
	{
		$str_courseId=base64_decode($courseId);
		if(!empty($instId)){
			$str_instId=base64_decode($instId);
			$inst=$this->section_m->getMembersInst($str_courseId,$str_instId);
		}
		else
			$inst=$this->section_m->getMembersInst($str_courseId);
		echo json_encode($inst);
	}

	public function getMembersStud($courseId,$sectionId)
	{
		$str_courseId=base64_decode($courseId);
		$str_sectionId=base64_decode($sectionId);
		$stud=$this->section_m->getMembersStud($str_courseId,$str_sectionId);
		if(!empty($stud))
			echo json_encode($stud);
	}

	public function assignInst($instId,$sectionId,$courseId) {
		$str_sectionId=base64_decode($sectionId);
		$str_instId=base64_decode($instId);
		$str_courseId=base64_decode($courseId);
		$data=array(
			"user_id"=> $str_instId,
			"section_id" => $str_sectionId
		);

		if($this->section_m->assignInst($data)){
			$status['val']=true;
			$status['msg']="Successfully assigned an instructor";
			echo json_encode($status);
		}

		else {
			$status['val']=false;
			$status['msg']="Technical Error! Something wrong with the backend system.";
			echo json_encode($status);
		}
	}

	public function changeInst($instId,$sectionId) {
		$str_sectionId=base64_decode($sectionId);
		$str_instId=base64_decode($instId);
		$data=array(
			"user_id"=> $str_instId
		);
		
		if($this->section_m->changeInst($str_instId,$str_sectionId,$data)){
			$status['val']=true;
			$status['msg']="Successfully changed an instructor";
			echo json_encode($status);
		}

		else {
			$status['val']=false;
			$status['msg']="Technical Error! Something is wrong with the backend part.";
			echo json_encode($status);
		}
	}

	//DATA TABLE STUFF <STUDENT>
	public function getNonSectStud($courseId,$sectionId)
	{
		$str_courseId=base64_decode($courseId);
		$str_sectionId=base64_decode($sectionId);
	   	$student=$this->section_m->getNonSectStud($str_courseId,$str_sectionId);
	   	if(!empty($student))
			   echo json_encode($student);
		else 
			print_r($str_courseId);
	}

	public function addStud()
	{
		extract($this->input->post());
		for ($i=0; $i < sizeof($id); $i++) { 
			$this->section_m->addStud(base64_decode($sectionId),base64_decode($id[$i]));
		}
		$status['val']=true;
		$status['msg']="Successfully added the new students";
		echo json_encode($status);
	}
	public function processRemoveStud($studentId,$sectionId)
	{
		$status = $this->initiateStat();//intiate status
		if($this->section_m->removeStud(base64_decode($studentId),base64_decode($sectionId))) {
			$status['val']=true;
			$status['msg']="success";
		}

		else {
			$status['val']=false;
			$status['msg']="fail!";
		}
		echo json_encode($status);
	}
}

/* End of file Section.php */
/* Location: ./application/controllers/Section.php */