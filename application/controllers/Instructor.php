<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*****************
Created on: 24/08/2019
Description: This controller is to manage the instructor CRUD
********************/
class Instructor extends My_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Instructor_model','instructor_m');
		$this->load->model('Section_model','sm');
		$this->load->library('form_validation');
		if(!$this->auth()) 
			redirect("login");
	}

	public function uni($type='') {
		$data['title']="List of university";
		$data["type"]=$type;
		echo $this->blade->view()->make("instructor.uni",$data);
	}

	//DATA TABLE STUFF <UNI>
	public function uniList($type)
	{
	   //fetch data
	   $fetch_data = $this->instructor_m->processUniListDB();  
       $data = array();
       foreach($fetch_data as $row)  
       {  
       		//SETTING THE BUTTONS FOR EDIT AND DELETE. FOR READABILITY
        	//COUNT COURSE
        	$course=$this->instructor_m->countInstUni($row->id);
	        if($type!="list"){
				$butt_view = "<a href='".base_url()."instructor/add/".base64_encode($row->id)."' data-toggle='tooltip' title='Add New Course' class='btn btn-success btn-sm'><i class='fas fa-plus-circle'></i></a>";
	        }

	        else {
	        	$butt_view = "<a href='".base_url()."instructor/list/".base64_encode($row->id)."' data-toggle='tooltip' title='View Instructors' class='btn btn-info btn-sm'><i class='fas fa-list-alt'></i></a>";
	        }

            $sub_array = array();
            $sub_array[] ="";   
            $sub_array[] = $row->name;
            $sub_array[] = $course->total;
            $sub_array[] = $butt_view;
            $data[] = $sub_array;  
        }  
       $output = array(  
            "draw" => intval($_POST["draw"]),  
            "recordsTotal" => $this->instructor_m->processUniListCount(),  
            "recordsFiltered" => $this->instructor_m->processUniListFiltered(),  
            "data" => $data  
       );  
       echo json_encode($output);
	}
	 
	public function getCourse($uniId)	{
		$data['course']=$this->instructor_m->getCourseDB(base64_decode($uniId));
		echo json_encode($data);
	}

	public function add($uniId) {
		if($this->instructor_m->countCourseUni(base64_decode($uniId))>0){
			$data['title']="Add New Instructor";
			$url=explode("/", $_SERVER['REQUEST_URI']);
			$length=count($url);
			$data['uniId']=$uniId;
			$data['uni']=$this->instructor_m->getUniName(base64_decode($uniId));
			echo $this->blade->view()->make("instructor.add",$data);
		}
		else{
			$data['uni']=$this->instructor_m->getUniName(base64_decode($uniId));
			$data['msg']="There are 0 courses on ".$data['uni']->name.". Please add at least one course";
			echo $this->blade->view()->make("instructor.nocourse",$data);
		}
		
	}

	public function processAdd() {

		$this->form_validation->set_rules('course', 'course', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('name', 'name', 'required');
		
		// form validation
		if(!$this->form_validation->run())
			echo $this->blade->view()->make("instructor.add");

		else {
			extract($this->input->post());//extract input
			$status = $this->initiateStat();//intiate status
			// $password=$this->genPassword();
			$password="12345678";
			$data=array(
				"name"=> strtoupper($name),
				"password"=> password_hash($password,PASSWORD_DEFAULT),
				"email"=> $email,
				"role_id"=> 2,
				"created_at"=> date('Y-m-d H:i:s'),
				"uni_id"=>base64_decode($uniId)
			);
			
			if($this->instructor_m->addInstDB($data,$course)){
				// $this->sendMail($email,$password,strtoupper($name));
				$status['val']=true;
				$status['msg']="Successfully added new instructor";
				echo json_encode($status);
			}
			else {
					$status['val']=false;
					$status['msg']="Technical Error!";
					echo json_encode($status);
			}
			
		}//end of else form validation
	}

	public function checkExist() {	
		if($this->instructor_m->checkExistDB($this->input->get('email'))>0)
			echo "false";
		else
			echo "true";
	}

	public function list($uniId)
	{
		$data['title']="List of Course";
		// $url=explode("/", $_SERVER['REQUEST_URI']);
		// $length=count($url);
		$data['uniId']=$uniId;
		$data['uni']=$this->instructor_m->getUniName(base64_decode($data['uniId']));
		echo $this->blade->view()->make("instructor.list",$data);
	}

	//DATA TABLE STUFF <Instructor>
	public function processList($uniId)
	{
		//fetch data
	   $str_uniId=base64_decode($uniId);
	   $fetch_data = $this->instructor_m->processListDB($str_uniId);  
       $data = array();
       foreach($fetch_data as $row)  
       {  
           	//SETTING THE BUTTONS FOR EDIT AND DELETE. FOR READABILITY

			$butt_delete = "<button type='button' data-toggle='tooltip' title='Delete instructor ' name='update' onclick='remove(\"".base64_encode($row->id)."\",\"".$row->name."\")' class='btn btn-danger btn-sm'><i class='fas fa-trash-alt'></i></button>";

			$section=$this->instructor_m->getTotalSections($row->id);

			$totalSection = ($section->total==0) ? 0 : $section->total;
			$course=$this->instructor_m->getCourseDB(base64_decode($uniId));
	        $sub_array = array();
	        $sub_array[] ="";   
	        $sub_array[] = $row->name;
	        $sub_array[] = $row->email;
			$sub_array[] = $course[0]->code;
			$sub_array[] = $totalSection;
	        $sub_array[] = $row->created_at;
	        $sub_array[] = $butt_delete;
	        $data[] = $sub_array;  
        }  
       $output = array(  
            "draw" => intval($_POST["draw"]),  
            "recordsTotal" => $this->instructor_m->processListCount($str_uniId),  
            "recordsFiltered" => $this->instructor_m->processListFiltered($str_uniId),  
            "data" => $data  
       );  
       echo json_encode($output);
	}

	public function processDelete($userId)
	{
		$status = $this->initiateStat();//intiate status
		$strUserId=base64_decode($userId);//string instId
		if($this->instructor_m->deleteDB($strUserId)) {
			$status['val']=true;
			$status['msg']="success";
		}

		else {
			$status['val']=false;
			$status['msg']="fail!";
		}
		echo json_encode($status);
	}
}