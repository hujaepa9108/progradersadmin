<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*****************
Created on: 24/08/2019
Description: This controller is to manage the student CRUD
********************/
class Student extends My_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Student_model','student_m');
		$this->load->library('form_validation');
		if(!$this->auth()) 
			redirect("login");
	}

	public function uni($type='') {
		$data['title']="List of university";
		$data["type"]=$type;
		echo $this->blade->view()->make("student.uni",$data);
	}

	//DATA TABLE STUFF <UNI>
	public function uniList($type)
	{
	   //fetch data
	   $fetch_data = $this->student_m->processUniListDB();  
       $data = array();
       foreach($fetch_data as $row)  
       {  
       		//SETTING THE BUTTONS FOR EDIT AND DELETE. FOR READABILITY
        	//COUNT COURSE
        	$student=$this->student_m->countStudentUni($row->id);
	        if($type!="list"){
				$butt_view = "<a href='".base_url()."student/add/".base64_encode($row->id)."' data-toggle='tooltip' title='Add New Course' class='btn btn-success btn-sm'><i class='fas fa-plus-circle'></i></a>";
	        }

	        else {
	        	$butt_view = "<a href='".base_url()."student/list/".base64_encode($row->id)."' data-toggle='tooltip' title='View students' class='btn btn-info btn-sm'><i class='fas fa-list-alt'></i></a>";
	        }

            $sub_array = array();
            $sub_array[] ="";   
            $sub_array[] = $row->name;
            $sub_array[] = $student->total;
            $sub_array[] = $butt_view;
            $data[] = $sub_array;  
        }  
       $output = array(  
            "draw" => intval($_POST["draw"]),  
            "recordsTotal" => $this->student_m->processUniListCount(),  
            "recordsFiltered" => $this->student_m->processUniListFiltered(),  
            "data" => $data  
       );  
       echo json_encode($output);
	}

	public function getCourse()	{
		$data['course']=$this->student_m->getCourseDB();
		echo json_encode($data);
	}

	public function add($uniId) {

		if($this->student_m->countCourseUni(base64_decode($uniId))>0) {
			$data['title']="Add New student";
			$data['uniId']=$uniId;
			$data['uni']=$this->student_m->getUniName(base64_decode($uniId));
			echo $this->blade->view()->make("student.add",$data);
		}

		else {
			$data['uni']=$this->student_m->getUniName(base64_decode($uniId));
			$data['msg']="There are 0 courses on ".$data['uni']->name.". Please add at least one course before proceed to add student";
			echo $this->blade->view()->make("student.nocourse",$data);
		}
	}

	public function processAdd() {

		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('idno', 'idno', 'required');
		
		// form validation
		if(!$this->form_validation->run())
			echo $this->blade->view()->make("student.add");

		else {
			extract($this->input->post());//extract input
			$status = $this->initiateStat();//intiate status
			$data=array(
				"name"=> strtoupper($name),
				"password"=> password_hash($idno,PASSWORD_DEFAULT),
				"email"=> $email,
				"role_id"=> 3,
				"created_at"=> date('Y-m-d H:i:s'),
				"uni_id"=>base64_decode($uniId)
			);
			
			if($this->student_m->addStudentDB($data,$course,$idno)){
				$status['val']=true;
				$status['msg']="Successfully added new student";
				echo json_encode($status);
			}
			else {
				$status['val']=false;
				$status['msg']="Technical Error!";
				echo json_encode($status);
			}
			
		}//end of else form validation
	}

	public function checkExist() {	
		if($this->student_m->checkExistDB($this->input->get('email'))>0)
			echo "false";
		else
			echo "true";
	}

	public function list($uniId)
	{
		$data['title']="List of Course";
		$url=explode("/", $_SERVER['REQUEST_URI']);
		$length=count($url);
		$data['uniId']=$uniId;
		$data['uni']=$this->student_m->getUniName(base64_decode($this->uri->segment($length-2)));
		echo $this->blade->view()->make("student.list",$data);
	}

	//DATA TABLE STUFF <Student>
	public function processList($uniId)
	{
		//fetch data
	   $str_uniId=base64_decode($uniId);
	   $fetch_data = $this->student_m->processListDB($str_uniId);  
       $data = array();
       foreach($fetch_data as $row)  
       {  
           	//SETTING THE BUTTONS FOR EDIT AND DELETE. FOR READABILITY

			$butt_delete = "<button type='button' data-toggle='tooltip' title='Delete student' name='delete' onclick='remove(\"".base64_encode($row->id)."\")' class='btn btn-danger btn-sm'><i class='fas fa-trash-alt'></i></button>";

	        $sub_array = array();
	        $sub_array[] ="";   
	        $sub_array[] = $row->name;
	        $sub_array[] = $row->email;
			$sub_array[] = $row->idno;
			$sub_array[] = $row->code;
	        $sub_array[] = "<i class='fas fa-clock'></i>&nbsp;".date("h:i:s A",strtotime($row->created_at))."&nbsp;&nbsp;<i class='fas fa-calendar-alt'></i>&nbsp;".date("d-M-Y",strtotime($row->created_at));
	        $sub_array[] = $butt_delete;
	        $data[] = $sub_array;  
        }  
       $output = array(  
            "draw" => intval($_POST["draw"]),  
            "recordsTotal" => $this->student_m->processListCount($str_uniId),  
            "recordsFiltered" => $this->student_m->processListFiltered($str_uniId),  
            "data" => $data  
       );  
       echo json_encode($output);
	}

	public function processDelete($userId)
	{
		$status = $this->initiateStat();//intiate status
		$strUserId=base64_decode($userId);//string studId
		if($this->student_m->deleteDB($strUserId)) {
			$status['val']=true;
			$status['msg']="success";
		}

		else {
			$status['val']=false;
			$status['msg']="fail!";
		}
		echo json_encode($status);
	}
}