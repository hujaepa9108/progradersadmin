<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*****************
Created on: 24/08/2019
Description: This controller is to manage the landing page, login and register form process. Authenticated user will be directed to Dashboard controller
********************/
class Login extends My_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Admin_model','admin_m');
		if($this->auth()) 
			redirect("dashboard");
	}

	public function index() {
		echo $this->blade->view()->make("login");
	}

	public function process(){

		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$status = $this->initiateStat();

		//if form validation failed!
		if($this->form_validation->run() == FALSE){
			echo $this->blade->view()->make("login");
		}

		else {
			$username=$this->input->post('username');
			$password=$this->input->post('password');
			$data=$this->initiateStat();

			$rec = $this->admin_m->loginDB($username);//fetch username and the encrypted password
			if(isset($rec->username) && password_verify($password,$rec->password)) { 
				//create token
				$token = $this->generateToken($rec->username);
				$sessdata = array(
					"user_id" => $this->encrypt($rec->id),
					'username'=>$rec->username,
					'token'=>$token
				);
				//set session
				$this->session->set_userdata($sessdata);
				redirect('dashboard');
			}

			else {//invalid username or password
				$data['status']="invalid username";
				$data['msg']="Login Failed! Invalid username or password";
				echo $this->blade->view()->make("login",$data);
			}
		}
	}
}