<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*****************
Created on: 24/08/2019
Description: This controller is to manage the landing page, login and register form process
********************/
class Dashboard extends My_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Dashboard_model", "dashboard_m");
		if(!$this->auth()) 
			redirect("login");
	}

	//LOAD DASHBOARD UI
	public function index() {
		$data['title']="Dashboard";
		$data["uni"]=$this->dashboard_m->totalUni(); //return total uni
		$data["course"]=$this->dashboard_m->totalCourse(); //return total course
		$data["inst"]=$this->dashboard_m->totalInst(); //return total instructor
		$data["student"]=$this->dashboard_m->totalStudent(); //return total student
		$data["assignment"]=$this->dashboard_m->totalAssignment(); //return total assignment
		$data["username"]=$this->decrypt($_SESSION["username"]);
		echo $this->blade->view()->make("dashboard",$data);
	}

	public function processList()
	{
		$fetch_data = $this->dashboard_m->processListDB();  
		$data = array();
		foreach($fetch_data as $row)  
		{ 

		    $sub_array = array();   
		    $sub_array[] = $row->name; 
			$sub_array[] = $row->role_name;  
			$sub_array[] = "<i class='fas fa-clock'></i>&nbsp;".date("H:i:s",strtotime($row->login));
		    $sub_array[] = "<i class='fas fa-calendar-alt'></i>&nbsp;".date("d-M-Y",strtotime($row->login));
		    $data[] = $sub_array;  
		}  
		$output = array(  
		    "draw" => intval($_POST["draw"]),  
		    "recordsTotal" => $this->dashboard_m->processListCount(),  
		    "recordsFiltered" => $this->dashboard_m->processListFiltered(),  
		    "data" => $data  
		);  
		echo json_encode($output);
	}

}