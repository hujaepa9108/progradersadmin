<?php
//namespace for coolpraz
use Coolpraz\PhpBlade\PhpBlade;

class MY_Controller extends CI_Controller{

	protected $views = APPPATH."views";
	protected $cache = APPPATH."cache";
	protected $blade;

	protected function __construct(){
		parent::__construct();
		$this->blade = new PhpBlade($this->views,$this->cache);
	}

	protected function initiateStat() {
		return array('val'=>'',
			'msg'=>'',
			'status'=>''
		);
	}
	protected function getAdminId()
	{
		$this->load->model("Admin_model","am");
		$data = $this->am->getAdminIdDB();
		return $data->admin_id;
	}
	protected function generateToken($username) {
		return sha1(date("Ymd").$username);
	}

	protected function checkToken() {
		return sha1(date("Ymd").$this->session->username);
	}

	protected function auth(){
		if(!empty($this->session->username) && $this->session->token===$this->checkToken())
			return true;
		else
			return false;
	}

	protected function encrypt($string)
	{
		$cipher_method="aes-128-ctr";
		$encrypt_key="rVR9ZpWy73#";
		$option=0;
		$encrypt_iv="1234567891011121";

		$crypted_str= openssl_encrypt($string, $cipher_method, $encrypt_key, $option, $encrypt_iv);

		return $crypted_str;
	}

	protected function decrypt($crypted_str)
	{
		$cipher_method="aes-128-ctr";
		$decrypt_key="rVR9ZpWy73#";
		$option=0;
		$decrypt_iv="1234567891011121";

		$decrypted_str=openssl_decrypt ($crypted_str, $cipher_method,  
        $decrypt_key, $option, $decrypt_iv); 

        return $decrypted_str;
	}

	protected function genPassword()
	{
		$length=6;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	protected function sendMail($email,$password,$name){
        // Load PHPMailer library
        $this->load->library('phpmailer_lib');
        
        // PHPMailer object
        $mail = $this->phpmailer_lib->load();
        
        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'prograders7@gmail.com';
        $mail->Password = '1q2w3e4r*#@';
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;
        
        $mail->setFrom('prograders7@gmail.com', 'Prograders Admin');
        // $mail->addReplyTo('info@example.com', 'CodexWorld');
        
        // Add a recipient
        $mail->addAddress($email);
        
        // Add cc or bcc 
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');
        
        // Email subject
        $mail->Subject = 'Successfully registered for Prograders';
        
        // Set email format to HTML
        $mail->isHTML(true);
        
        // Email body content
        $mailContent = "<h1>Please login using the following credentials</h1>
            <p>Email : ".$email." </p>
            <p>Name : ".$name." </p>
            <p>Password : ".$password."</p>";
        $mail->Body = $mailContent;
        
        // Send email
        // if(!$mail->send()){
        //     echo 'Message could not be sent.';
        //     echo 'Mailer Error: ' . $mail->ErrorInfo;
        // }else{
        //     echo 'Message has been sent';
        // }
    }
}

?>