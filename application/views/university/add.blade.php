@extends('layouts.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>{{ $title }} <i class="fas fa-university"></i></h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
        <div class="invoice p-3 mb-3">
          <div id="overlay">
            <div id="overlay-text">Processing....</div>
          </div>
            <form role="form" id="add_university">
              <div class="card-body">

               <div class="form-group row">
                <div class="col-sm-12">
                  <label for="exampleInputEmail1">University name</label> 
                  <input type="text" id="university" name="university" placeholder="" required="required" class="form-control">
                  <span id="university_exist"></span>
                </div>
               </div>

               <button type="submit" class="btn btn-success">Submit</button> 
               <button type="reset" class="btn btn-danger">Clear</button>

              </div>
           </form>
          <script src="{{ base_url() }}assets/myscript/university/add.js"></script>
          
        </div>
      </div>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
@endsection