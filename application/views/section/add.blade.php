@extends('layouts.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{base_url()}}section/uni">Go Back To University List</a></li>
              <li class="breadcrumb-item"><a href="{{base_url()}}section/course/{{$uniId}}">View Courses For {{$uni->name}}</a></li>
              <li class="breadcrumb-item"><a href="{{base_url()}}section/list/{{$uniId}}/{{$courseId}}">View Section List For {{$course->code}} </a></li>
              <li class="breadcrumb-item active">Create New Section</li>
            </ol>
          </nav>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-10">
        <div class="invoice p-3 mb-3">

          <div id="overlay">
            <div id="overlay-text">Processing....</div>
          </div>

          <div id="inst">
            <form role="form" id="add_section">
              <div class="card-body">
                {{-- <h3>Add New Section for {{$course->code}}</h3> --}}
               <div class="form-group row">
                <div class="col-sm-12">
                  <label for="exampleInputEmail1"><h3>Add New Section for {{$course->code}} <i class="text-primary">({{$course->name}})</i></h3></label> 
                  <input type="text" id="section" name="section" placeholder="Enter section name" required="required" class="form-control">
                </div>
               </div>

               <button type="submit" class="btn btn-success">Add</button> 
               <button type="reset" class="btn btn-danger">Clear</button>

              </div>
           </form>
         </div>
        <script src="{{ base_url() }}assets/myscript/section/add.js"></script>
          
        </div>
      </div>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
@endsection