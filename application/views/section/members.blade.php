@extends('layouts.master')

@section('content')
{{-- datatable stuff --}}
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{base_url()}}section/uni">Go Back To University List</a></li>
              <li class="breadcrumb-item" aria-current="page"><a href="{{base_url()}}section/course"> Go back to course list for {{$uni->name}} </a></li>
              <li class="breadcrumb-item" aria-current="page"><a href="{{base_url()}}section/list/{{$uniId}}/{{$courseId}}"> Go back to section list</a></li>
              <li class="breadcrumb-item active" aria-current="page">View Instructor/Students</li>
            </ol>
          </nav>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
        <div class="invoice p-3 mb-3">
          <div id="overlay">
            <div id="overlay-text">Processing....</div>
          </div>

          <div class="float-left">
            <a class="btn btn-default" href="{{base_url()}}section/list/{{$uniId}}/{{$courseId}}"><i class="nav-icon fas fa-arrow-circle-left"></i> Back</a>
          </div>
          <br>

        {{-- INSTRUCTOR --}}
        <div class="table-responsive">
          <hr>
          <table border="0">
            <tr>
              <th><h4><u>Assigned Instructor</u></h4></th>
            </tr>

            <tr align="left">
              @if(isset($inst->name))
                <td>
                  <b><i class="text-primary">{{$inst->name}}</i></b> <button class="btn btn-warning btn-sm changeInst" data-toggle="tooltip" data-placement="bottom" title="Change Instructor"><i class="fas fa-random"></i></button>
                  <input type="hidden" id='instId' value="{{$instId}}">
                </td>
              @else
                <td><b><i class="text-muted">No Instructor Assigned</i></b> <button class="btn btn-success btn-sm assignInst" data-toggle="tooltip" data-placement="bottom" title="Add Instructor"><i class="fas fa-plus"></i></button></td>
              @endif
            </tr>
          </table>
        </div>

        {{-- STUDENT --}}
        <div class="table-responsive">
          <hr>
          <div class="float-left">
            <h4><u>List of Students</u></h4>
          </div>
          <div class="float-right">
            <a class="btn btn-success assignStud" href="{{base_url()}}section/getMembersStud/{{$sectionId}}"><i class="nav-icon fas fa-plus-circle"></i> Assign New Students </a>
          </div>
            <br/>
            <br/>
            <table id="student_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                <thead class="thead-light">
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Metric No</th>
                        <th>Created At</th>
                        <th>Manage</th>
                    </tr>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>  
            </table>
        </div> 
        {{-- end of student list--}}

          <script src="{{ base_url() }}assets/myscript/section/members.js"></script>
        </div>
      </div>
      </div>
    </div>
  </section>
</div>

<!-- ASSIGNED INSTRUCTOR -->
<div class="modal fade" id="inst_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Assigning Instructor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table id="inst_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                <thead class="thead-light">
                    <tr>
                        <th width="10%">No</th>
                        <th width="70%">Instructor Name</th>
                        <th width="20%">Manage</th>
                    </tr>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>  
            </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- ASSIGNED INSTRUCTOR -->
<div class="modal fade" id="inst_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Assigning Students</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table id="inst_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                <thead class="thead-light">
                    <tr>
                        <th width="10%">No</th>
                        <th width="70%">Instructor Name</th>
                        <th width="20%">Manage</th>
                    </tr>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>  
            </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- ASSIGNED STUDENTS -->
<div class="modal fade" id="stud_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Assigning Students</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table id="stud_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                <thead class="thead-light">
                    <tr>
                        <th width="10%">No</th>
                        <th width="70%">Student Name</th>
                        <th width="70%">Student Metric No</th>
                        <th width="20%">Manage</th>
                    </tr>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>  
            </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success addStud">Assigned</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- /.content-wrapper -->
@endsection

