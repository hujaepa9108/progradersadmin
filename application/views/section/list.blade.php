@extends('layouts.master')

@section('content')
{{-- datatable stuff --}}
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{base_url()}}section/uni">Go Back To University List</a></li>
              <li class="breadcrumb-item" aria-current="page"><a href="{{base_url()}}section/course/{{$uniId}}"> Go back to course list for {{$uni->name}} </a></li>
              <li class="breadcrumb-item active" aria-current="page">All sections for {{$course->code}}</li>
            </ol>
          </nav>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
        <div class="invoice p-3 mb-3">
          <div id="overlay">
            <div id="overlay-text">Processing....</div>
          </div>

          <div class="float-left">
            <a class="btn btn-default" href="{{base_url()}}section/course/{{$uniId}}"><i class="nav-icon fas fa-arrow-circle-left"></i> Back</a>
          </div>

          <div class="float-right">
            <a class="btn btn-success" href="{{base_url()}}section/add/{{$uniId}}/{{$courseId}}"><i class="nav-icon fas fa-plus-circle"></i> Add New</a>
          </div>

        <div class="table-responsive">
          <hr>
          <h4>Section List for {{$course->code}}</h4>

            <table id="section_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                <thead class="thead-light">
                    <tr>
                        <th>No</th>
                        <th>Section</th>
                        <th>Assigned Instructor</th>
                        <th>Total Student</th>
                        <th>Created At</th>
                        <th>Manage</th>
                    </tr>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>  
            </table>
        </div>{{-- end of table responsive --}}
        <script src="{{ base_url() }}assets/myscript/section/list.js"></script>
          
        </div>
      </div>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
@endsection

