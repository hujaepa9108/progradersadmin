@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          {{-- <h1>{{ $title }} <i class="fas fa-book"></i></h1> --}}
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{base_url()}}course/uni">Go Back To University List</a></li>
              <li class="breadcrumb-item"><a href="{{base_url()}}course/list/{{$uniId}}/{{base64_encode($uniName)}}">View Courses for {{$uniName}}</a></li>
              <li class="breadcrumb-item active" aria-current="page">Add New Course</li>
            </ol>
          </nav>        
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
        <div class="invoice p-3 mb-3">
          <h3>{{ $title }} <i class="fas fa-book"></i></h3>
          <hr>
          <div id="overlay">
            <div id="overlay-text">Processing....</div>
          </div>
            <form role="form" id="add_course">
              <div class="card-body">

               <div class="form-group row">
                <div class="col-sm-12">
                  <label for="exampleInputEmail1">Course Code <sup>*</sup></label> 
                  <input type="text" id="code" name="code" placeholder="Enter course code" class="form-control">
                  <span id="course_exist"></span>
                </div>
               </div>

              <div class="form-group row">
                <div class="col-sm-12">
                  <label for="exampleInputEmail1">Course Name <sup>*</sup></label> 
                  <input type="text" id="name" name="name" placeholder="Enter the course name" class="form-control">
                </div>
              </div>

               <div class="form-group row">
                  <div class="col-sm-12">
                    <label for="exampleInputEmail1">Course Description <sup>*</sup></label> 
                    <textarea class="form-control" name="description" id="description" cols="30" rows="5"></textarea>
                  </div>
               </div>

               <div class="form-group row">
                  <div class="col-sm-12">
                    <label for="exampleInputEmail1">Course Level <sup>*</sup></label> 
                    <select name="level" id="level" class="form-control">
                      <option value="">Please select course level</option>
                      <option value="postgraduate">Postgraduate</option>
                      <option value="bachelor/degree">Bachelor/Degree</option>
                      <option value="diploma">Diploma</option>
                      <option value="foundation">Foundation</option>
                      <option value="matriculation">Matriculation</option>
                    </select>
                  </div>
               </div>

               <div class="form-group row">
                <div class="col-sm-12">
                  <label>Programming Language <sup>*</sup></label>
                  <div id="prog_lang">
                    <select name="language" id="language" class="form-control pl_name">
                      <option value="">Please select the programming language</option>
                      <option v-for="item in prog_lang" v-bind:value="item.id">@{{item.name}}</option>
                    </select>
                  </div>
                </div>
               </div>

               <input type="hidden" id="pl_name_hidden" name="pl_name_hidden">
               <button type="submit" class="btn btn-success">Submit</button> 
               <button type="reset" class="btn btn-danger">Clear</button>
               <input type="hidden" id="uniId" name="uniId">
              </div>
           </form>
          <script src="{{ base_url() }}assets/myscript/course/add.js"></script>
          
        </div>
      </div>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
@endsection