@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1 class="text-primary">Edit Course Info :  {{$course->code}} ({{$course->name}}) <i class="fas fa-pencil-alt"></i></h1>        
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
        <div class="invoice p-3 mb-3">
          <a href="{{base_url()}}course/list/{{base64_encode($course->university_id)}}" class="btn btn-default"><i class="fas fa-arrow-circle-left"></i> Back</a>
          <hr>
          <div id="overlay">
            <div id="overlay-text">Processing....</div>
          </div>
            <form role="form" id="add_course">
              <div class="card-body">
               <div class="form-group row">
                <div class="col-sm-12">
                  <label for="exampleInputEmail1">Course Code <sup>*</sup></label> 
                  <input type="text" id="code" name="code" placeholder="Enter course code" class="form-control" value="{{$course->code}}">
                  <span id="course_exist"></span>
                </div>
               </div>

              <div class="form-group row">
                <div class="col-sm-12">
                  <label for="exampleInputEmail1">Course Name <sup>*</sup></label> 
                  <input type="text" id="name" name="name" placeholder="Enter the course name" class="form-control" value="{{$course->name}}">
                </div>
              </div>

               <div class="form-group row">
                  <div class="col-sm-12">
                    <label for="exampleInputEmail1">Course Description <sup>*</sup></label> 
                    <textarea class="form-control" name="description" id="description" cols="30" rows="5">{{$course->description}}</textarea>
                  </div>
               </div>

               <div class="form-group row">
                  <div class="col-sm-12">
                    <label for="exampleInputEmail1">Course Level</label> 
                    <select name="level" id="level" class="form-control">
                      <option value="postgraduate" {{ ($course->uni_level == "postgraduate") ? "selected" : "" }}>Postgraduate</option>
                      <option value="bachelor/degree" {{ ($course->uni_level == "bachelor/degree") ? "selected" : "" }}>Bachelor/Degree</option>
                      <option value="diploma" {{ ($course->uni_level == "diploma") ? "selected" : "" }}>Diploma</option>
                      <option value="foundation" {{ ($course->uni_level == "foundation") ? "selected" : "" }}>Foundation</option>
                      <option value="matriculation" {{ ($course->uni_level == "matriculation") ? "selected" : "" }}>Matriculation</option>
                    </select>
                  </div>
               </div>

               <div class="form-group row">
                <div class="col-sm-12">
                  <input type="hidden" name="language_id" id="language_id" value={{base64_encode($course->pl_id)}}/>
                  <label>
                    Curent Programming Language/Compiler: <span class="text-success" id="language_name_display"><i><b>{{$course->pl_name}}</b></i></span>
                    <input type="hidden" name="language_name" id="language_name" value="{{$course->pl_name}}"/>
                  </label>
                  <div id="prog_lang">
                    <select name="choose_language" id="choose_language" class="form-control">
                      <option value="">Choose to change the language</option>
                      <option v-for="item in prog_lang" v-bind:value="item.id">@{{item.name}}</option>
                    </select>
                  </div>
                </div>
               </div>

               <button type="submit" class="btn btn-warning">Edit</button> 
               <button type="reset" class="btn btn-danger">Clear</button>

              </div>
           </form>
          <script src="{{ base_url() }}assets/myscript/course/edit.js"></script>
          
        </div>
      </div>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
@endsection