@extends('layouts.master')

@section('content')
{{-- datatable stuff --}}
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
   <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
        <div class="invoice p-3 mb-3">

          <h5>{{$msg}}</h5>
          </div>
          
        
          
        </div>
      </div>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
@endsection

