@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1>{{ $title }}</h1>
          <hr>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3>{{ $uni->total }}</h3>

              <p>Universities</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-bag"></i> -->
              {{-- <i class="nav-icon fas fa-copy"></i> --}}
            </div>
            <a href="{{base_url()}}university/list" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3>{{ $course->total }}</h3>

              <p>Courses</p>
            </div>
            <div class="icon">
              {{-- <i class="ion ion-stats-bars"></i> --}}
            </div>
            <a href="{{ base_url() }}course/uni/list" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-teal">
            <div class="inner">
              <h3>{{ $inst->total }}</h3>

              <p>Instructors</p>
            </div>
            <div class="icon">
              {{-- <i class="ion ion-person-add"></i> --}}
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-indigo">
            <div class="inner">
              <h3>{{ $student->total }}</h3>

              <p>Students</p>
            </div>
            <div class="icon">
              {{-- <i class="ion ion-pie-graph"></i> --}}
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h3>{{ $assignment->total }}</h3>

              <p>Assignments</p>
            </div>
            <div class="icon">
              {{-- <i class="ion ion-pie-graph"></i> --}}
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>

      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-12">
              <h1>Current Logged in Users</h1>
              <hr>
              {{-- WHITE BACKGROUND --}}
              <div class="invoice p-3 mb-3">
                <div class="table-responsive">
                    <table id="logged_in_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                        <thead class="thead-light">
                          <tr>
                              <th>Name</th>
                              <th>Role</th>
                              <th>Time login</th>
                              <th>Date</th>
                          </tr>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>  
                    </table>
                </div>
                {{-- end of table--}}
              <script src="{{ base_url() }}assets/myscript/dashboard/dashboard.js"></script>
              </div>
              {{-- END OF WHITE BACKGROUND --}}
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </div>

  
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>
@endsection