
<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-success elevation-4">
    <!-- Brand Logo -->
    <a href="{{ base_url() }}" class="brand-link navbar-teal" style="text-align: center">
      {{-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> --}}

      <span class="brand-text font-weight-light" style="color:white;"><i class="fas fa-user-cog"></i> Admin Zone</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <!-- <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"> -->
        </div>
        <div class="info">
          <span>Welcome <b class="text-primary">{{ $_SESSION["username"] }}</b></span>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          {{-- DASHBOARD --}}
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="fas fa-signal"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{base_url()}}" class="nav-link">
                  <i class="nav-icon fas fa-list-alt"></i>
                  <p>Go To Dashboard</p>
                </a>
              </li>
            </ul>
          <hr>
          </li>

          {{-- UNIVERSITIES --}}
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="fas fa-university"></i>
              <p>
                Universities
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ base_url() }}university/add" class="nav-link">
                  <i class="nav-icon fas fa-plus-square"></i>
                  <p>Add University</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{base_url()}}university/list" class="nav-link">
                  <i class="nav-icon fas fa-list-alt"></i>
                  <p>University List</p>
                </a>
              </li>
            </ul>
          <hr>
          </li>

          <!-- COURSE -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="fas fa-book"></i>
              <p>
                Courses
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="{{ base_url() }}course/uni" class="nav-link">
                  <i class="nav-icon fas fa-plus-square"></i>
                  <p>Add Course</p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="{{ base_url() }}course/uni/list" class="nav-link">
                  <i class="nav-icon fas fa-list-alt"></i>
                  <p>View Courses</p>
                </a>
              </li>
            </ul>
            <hr>
          </li>
          
          <!-- INSTRUCTOR -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="fas fa-user-tie"></i>
              <p>
                Instructors
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="{{ base_url() }}instructor/uni" class="nav-link">
                  <i class="nav-icon fas fa-plus-square"></i>
                  <p>Add Instructor</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{base_url()}}instructor/uni/list" class="nav-link">
                  <i class="nav-icon fas fa-list-alt"></i>
                  <p>View Instructors</p>
                </a>
              </li>

            </ul>
          <hr>
          </li>

          <!-- STUDENT -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="fas fa-user-graduate"></i>
              <p>
                Students
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="{{ base_url() }}student/uni" class="nav-link">
                  <i class="nav-icon fas fa-plus-square"></i>
                  <p>Add Student</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{base_url()}}student/uni/list" class="nav-link">
                  <i class="nav-icon fas fa-list-alt"></i>
                  <p>View Students</p>
                </a>
              </li>

            </ul>
            <hr>
          </li>

          <!-- SECTIONS -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-chalkboard-teacher"></i>
              <p>
                Class Sections
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              {{-- <li class="nav-item">
                <a href="{{base_url()}}section/add" class="nav-link">
                  <i class="nav-icon fas fa-plus-square""></i>
                  <p>Create New Section</p>
                </a>
              </li> --}}
              <li class="nav-item">
                <a href="{{base_url()}}section/uni" class="nav-link">
                  <i class="nav-icon fas fa-list-alt"></i>
                  <p>View Sections</p>
                </a>
              </li>

            </ul>
          <hr>
          </li>
                      
          <!-- ASSIGNMENTS -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-chalkboard-teacher"></i>
              <p>
                Assignments
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-list-alt"></i>
                  <p>View Assignments</p>
                </a>
              </li>

            </ul>
          <hr>
          </li>
        
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
