
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a></a>
      </li>
      <li class="nav-item">
        <a class="navbar-brand" href="{{ base_url() }}">
            <img src="{{ base_url() }}assets/img/prograders.png" width="150" height="30" alt="">
        </a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-cog"></i> More Settings
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">
            <i class="fas fa-user-edit"></i>
              Change Password
          </a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ base_url() }}logout">
            <i class="fas fa-sign-out-alt"></i>
            Logout
          </a>
        </div>
    </ul>
  </nav>
  <!-- /.navbar -->