@extends('layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          {{-- <h1>{{ $title }} <i class="fas fa-book"></i></h1> --}}
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{base_url()}}instructor/uni">Go Back To University List</a></li>
              <li class="breadcrumb-item"><a href="{{base_url()}}instructor/list/{{$uniId}}">View Instructors for {{$uni->name}}</a></li>
              <li class="breadcrumb-item active" aria-current="page">Add New Course</li>
            </ol>
          </nav>        
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
        <div class="invoice p-3 mb-3">
          <h3>{{ $title }} <i class="fas fa-book"></i></h3>
          <hr>
          <div id="overlay">
            <div id="overlay-text">Processing....</div>
          </div>
            <form role="form" id="add_instructor">
              <div class="card-body">

            <div class="form-group row">
              <div class="col-sm-12">
                <label>Course <sup>*</sup></label>
                <div id="course_list">
                  <select name="course" id="course" class="form-control">
                    <option value="">Please select the course</option>
                    <option v-for="item in course" v-bind:value="item.id">@{{item.code}} (@{{item.name}})</option>
                  </select>
                </div>
              </div>
             </div>

            <div class="form-group row">
              <div class="col-sm-12">
                <label for="exampleInputEmail1">Email <sup>*</sup></label> 
                <input type="email" id="email" name="email" placeholder="Enter user email" class="form-control">
              </div>
            </div>

             <div class="form-group row">
              <div class="col-sm-12">
                <label for="exampleInputEmail1">Name <sup>*</sup></label> 
                <input type="text" id="name" name="name" placeholder="Enter instructor name" class="form-control">
              </div>
             </div>
                
                
             <button type="submit" class="btn btn-success">Submit</button> 
             <button type="reset" class="btn btn-danger">Clear</button>
             <input type="hidden" id="uniId" name="uniId">
            </div>

           </form>
          <script src="{{ base_url() }}assets/myscript/instructor/add.js"></script>
          
        </div>
      </div>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
@endsection