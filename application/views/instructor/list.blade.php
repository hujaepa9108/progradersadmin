@extends('layouts.master')

@section('content')
{{-- datatable stuff --}}
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{base_url()}}instructor/uni/list">Go Back To University List</a></li>
              <li class="breadcrumb-item active" aria-current="page">All instructors for {{$uni->name}}</li>
            </ol>
          </nav>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
   <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
        <div class="invoice p-3 mb-3">

          <div id="overlay">
            <div id="overlay-text">Processing....</div>
          </div>
          <input type="hidden" name="uniId" value="{{$uniId}}">
          <div class="float-left">
            <a class="btn btn-default" href="{{base_url()}}instructor/uni/list"><i class="nav-icon fas fa-arrow-circle-left"></i> Back</a>
          </div>

          <div class="float-right">
            <a class="btn btn-success" href="{{base_url()}}instructor/add/{{$uniId}}"><i class="nav-icon fas fa-plus-circle"></i> Add New</a>
          </div>
          
        <div class="table-responsive">
          <hr>
            <table id="course_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                <thead class="thead-light">
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Course Code</th>
                        <th>Total Assigned Sections</th>
                        <th>Created At</th>
                        <th>Manage</th>
                    </tr>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>  
            </table>
        </div>{{-- end of table responsive --}}
        <script src="{{ base_url() }}assets/myscript/instructor/list.js"></script>
          
        </div>
      </div>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
@endsection

