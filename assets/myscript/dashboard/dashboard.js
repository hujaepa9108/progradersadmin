$(document).ready(function() {
  $('#logged_in_list').DataTable({  
       processing:true,  
       serverSide:true,
       order:[],  
       ajax:{  
            url:$("#url").val()+"dashboard/processList",  
            type:"post"  
       },
       language: {
        searchPlaceholder: "Enter name"
       }, 
       columnDefs:[  
	        {  
	             targets:[0,1,2,3],  
	             orderable:false,  
	        },  
       ],  
       "ordering": false
  });//end of data table
});//end of document ready