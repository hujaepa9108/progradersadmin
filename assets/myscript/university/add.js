$(document).ready(function() {
    //OVERLAY STUFF
    function setOverlay(){
        $("#overlay").css({
            display : "block"
        });
    }

    function offOverlay() {
        setTimeout(function() {
            $("#overlay").css({
                display : "none"
            });
        },3000);
    }
    //END OF OVERLAY STUFF//
    
    //FORM VALIDATION//
    $("#add_university").validate({
        //ERROR HANDLING//
        ignore: [], //enable validation on hidden field
        highlight: function(element) {
            $(element).closest('.form-control').addClass('is-invalid');
        },

        unhighlight: function(element) {
            $(element).closest('.form-control').removeClass('is-invalid');
        },

        errorElement: 'span',

        errorClass: 'badge badge-danger',
        
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) 
                error.insertAfter(element.parent());
            else 
                error.insertAfter(element);
        },
        //END OF ERROR HANDLING//
         
        rules:{
            university:{
                required: true,
                remote:  $("#url").val()+"university/ajaxUniExist"// to check exist code
            }
        },
        messages: {
            university:{
                remote: "This university has been added!"//set message
            }
        },
        submitHandler: function(form,event) {
            event.preventDefault();
            var url = $("#url").val() + "university/processAdd";//url setup
            var data = $(form).serialize();//form serialize for jquery validation
            setOverlay();
            $.ajax({
                url: url,
                type: 'post',
                beforeSend: function() {
                    setOverlay();
                }, 
                dataType: 'json',
                data: data,
                success: function(res){
                    offOverlay();
                    if(res.val)
                        window.location.href=$("#url").val()+"university/list";
                    else
                        bootbox.alert("Error");
                }
            }).fail(function(jqXHR,status,errorThrown) {
                console.log("Error : "+errorThrown);
            });
        }
    });
    //END OF FORM VALIDATION//
    
});