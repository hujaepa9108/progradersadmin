//fetch data to populate instructor list
$(document).ready(function() {
  $('#uni_list').DataTable({  
       processing:true,  
       serverSide:true,
       order:[],  
       ajax:{  
            url:$("#url").val()+"university/processList",  
            type:"post"  
       },
       language: {
        searchPlaceholder: "Enter uni name"
       },
       // pageLength: 2,
       ordering: false,
       columns: [
          {render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
          }},
          null,
          null,
          null
      ]
  });//end of data table
});//end of document ready


//OVERLAY STUFF
function setOverlay(){
    $("#overlay").css({
        display : "block"
    });
}

function offOverlay() {
    setTimeout(function() {
        $("#overlay").css({
            display : "none"
        });
    },500);
}

function remove(id,name){
  bootbox.confirm({ 
    size: "medium",
    message: "<span class='bootbox-custom-format'>Are you sure you want to delete this data row \""
    +name+"\" ? </span>",
    callback: function(result){
      if(result)
        removeAjax(id);
    }
  });
}

function removeAjax(id){
  var formData =  {
    id: id
  };
  var url = $("#url").val() + "university/processDelete/"+id;//url setup
  $.ajax({
      url: url,
      type: 'post',
      beforeSend: function() {
          setOverlay();
      }, 
      dataType: 'json',
      data: formData,
      success: function(res){
        console.log(res);
          offOverlay();
          if(res.val)
            location.reload();
            // console.log(res);
          else
            alert(res.msg);
      }
  }).fail(function(jqXHR,status,errorThrown) {
      console.log("Error : "+errorThrown);
  });
}


