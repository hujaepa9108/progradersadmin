
$(document).ready(function() {
    //OVERLAY STUFF
    function setOverlay(){
        $("#overlay").css({
            display : "block"
        });
    }

    function offOverlay() {
        setTimeout(function() {
            $("#overlay").css({
                display : "none"
            });
        },3000);
    }
    //END OF OVERLAY STUFF//
    
    //FORM VALIDATION//
    $("#add_course").validate({
        //ERROR HANDLING//
        ignore: [], //enable validation on hidden field
        highlight: function(element) {
            $(element).closest('.form-control').addClass('is-invalid');
        },

        unhighlight: function(element) {
            $(element).closest('.form-control').removeClass('is-invalid');
        },

        errorElement: 'span',

        errorClass: 'badge badge-danger',
        
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) 
                error.insertAfter(element.parent());
            else 
                error.insertAfter(element);
        },
        //END OF ERROR HANDLING//
         
        rules:{
            code:{
                required: true,
                remote:  $("#url").val()+"course/checkExist"// to check exist code
            },
            name:{
                required: true
            },
            description:{
                required: true
            },
            level:{
                required:true
            },
            language:{
                required:true
            }
        },
        messages: {
            code:{
                remote: "This course has been added!"//set message
            }
        },
        submitHandler: function(form,event) {
            event.preventDefault();
            var url = $("#url").val() + "course/processAdd";//url setup
            var data = $(form).serialize();//form serialize for jquery validation
            setOverlay();
            $.ajax({
                url: url,
                type: 'post',
                beforeSend: function() {
                    setOverlay();
                }, 
                dataType: 'json',
                data: data,
                success: function(res){
                    offOverlay();

                    if(res.val){

                        var url=window.location.href.split("/");
                        var uniId=url[url.length-2];
                        var uniName=url[url.length-1];
                        window.location.href=$("#url").val()+"course/list/"+uniId+"/"+uniName;
                    }
                    else
                        bootbox.alert(res.msg);
                    // alert(res.msg);
                }
            }).fail(function(jqXHR,status,errorThrown) {
                console.log("Error : "+errorThrown);
            });
        }
    });
    //END OF FORM VALIDATION//
    

    //fetch programming language
    var vuejs = new Vue({
        el:"#prog_lang",
        data: {
            prog_lang:[]
        },
        mounted: function() {       
            // var url = "https://judge0.p.rapidapi.com/languages";
            // // let token ="35abad9fa6msh9ac3a4e4c74c8edp15f100jsn11dbf8f7bb9b";
            // let config = {
            //   headers: {
            //     "x-rapidapi-host": "judge0.p.rapidapi.com",
            //     "x-rapidapi-key": "35abad9fa6msh9ac3a4e4c74c8edp15f100jsn11dbf8f7bb9b"
            //   }
            // };
            var url = "http://www.prograders.org:8080/languages";
            axios
                .get(url)
                .then(res => {
                    if(res.data.length>0){
                        this.prog_lang=res.data;
                    }
                });
        }
    });

    //GET URL PARAMETER VALUE
    var url=window.location.href.split("/");
    var getLastParam=url[url.length-2];
    $("#uniId").val(getLastParam);

    //SET PROGRAMMING LANGUAGE NAME FROM JUDGE0 API
    
    $('.pl_name').on('change',function(){
       $("#pl_name_hidden").val(this.options[this.selectedIndex].text);
    });
    // var pl_name=$(".pl_name selected").text();
    // alert(pl_name);
});