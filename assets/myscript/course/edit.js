$(document).ready(function(){
    //OVERLAY STUFF
    function setOverlay(){
        $("#overlay").css({
            display : "block"
        });
    }

    function offOverlay() {
        setTimeout(function() {
            $("#overlay").css({
                display : "none"
            });
        },3000);
    }
    //END OF OVERLAY STUFF//
    $("#choose_language").on("change",function(){
        $("#language_id").val($(this).children("option:selected").val());
        $("#language_name_display").text($(this).children("option:selected").text());
        $("#language_name").val($(this).children("option:selected").text());
    });


    //GET COURSE ID
    var url=window.location.href.split("/");
    var courseId=url[url.length-1];

    //GET UNI ID
    var url=window.location.href.split("/");
    var uniId=url[url.length-2];

    //FORM VALIDATION//
    $("#add_course").validate({
        //ERROR HANDLING//
        ignore: [], //enable validation on hidden field
        highlight: function(element) {
            $(element).closest('.form-control').addClass('is-invalid');
        },

        unhighlight: function(element) {
            $(element).closest('.form-control').removeClass('is-invalid');
        },

        errorElement: 'span',

        errorClass: 'badge badge-danger',
        
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) 
                error.insertAfter(element.parent());
            else 
                error.insertAfter(element);
        },
        //END OF ERROR HANDLING//
         
        rules:{
            code:{
                required: true,
                remote:  $("#url").val()+"course/checkExistEdit/"+courseId// to check exist code
            },
            name:{
                required: true
            },
            description:{
                required: true
            },
            level:{
                required:true
            },
            language:{
                required:true
            }
        },
        messages: {
            code:{
                remote: "This course has been added!"//set message
            }
        },
        submitHandler: function(form,event) {
            event.preventDefault();
            var url = $("#url").val() + "course/processEdit";//url setup
            var data = $(form).serialize()+"&courseId="+courseId;//form serialize for jquery validation
            console.log(data);
            setOverlay();
            $.ajax({
                url: url,
                type: 'post',
                beforeSend: function() {
                    setOverlay();
                }, 
                dataType: 'json',
                data: data,
                success: function(res){
                    offOverlay();

                    if(res.val){
                        window.location.href=$("#url").val()+"course/list/"+uniId;
                    }
                    else
                        bootbox.alert(res.msg);
                    // alert(res.msg);
                }
            }).fail(function(jqXHR,status,errorThrown) {
                console.log("Error : "+errorThrown);
            });
        }
    });
    //END OF FORM VALIDATION//
});//end of document ready

//fetch programming language
var vuejs = new Vue({
    el:"#prog_lang",
    data: {
        prog_lang:[]
    },
    mounted: function() {       
        var url = "http://www.prograders.org:8080/languages";
        axios
            .get(url)
            .then(res => {
                if(res.data.length>0){
                    this.prog_lang=res.data;
                }
            });
    }
});
