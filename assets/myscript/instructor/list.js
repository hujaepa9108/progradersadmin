//fetch data to populate course list
$(document).ready(function() {
  var url=window.location.href.split("/");
  var uniId=url[url.length-1];
  $('#course_list').DataTable({  
       processing:true,  
       serverSide:true,
       order:[],  
       ajax:{  
            url:$("#url").val()+"instructor/processList/"+uniId,  
            type:"post"  
       },
       language: {
        searchPlaceholder: "by 'email' "
       },
       ordering: false,
       columns: [
        {
          render: function (data, type, row, meta) {
                 return meta.row + meta.settings._iDisplayStart + 1;
          }
        },
          null,
          null,
          null,
          null,
          null,
          null
      ] 
  });//end of data table
});//end of document ready

//OVERLAY STUFF
function setOverlay(){
    $("#overlay").css({
        display : "block"
    });
}

function offOverlay() {
    setTimeout(function() {
        $("#overlay").css({
            display : "none"
        });
    },500);
}

function remove(instId){
  bootbox.confirm({ 
    size: "medium",
    message: "<span class='bootbox-custom-format'>Are you sure you want to <span class='bootbox-red-text'>delete</span> this instructor ? </span>",
    callback: function(result){
      if(result)
        removeAjax(instId);
    }
  });
}

function removeAjax(instId){
  var formData =  {
    instId: instId
  };

  var url = $("#url").val() + "instructor/processDelete/"+instId;//url setup
  $.ajax({
      url: url,
      type: 'post',
      beforeSend: function() {
          setOverlay();
      }, 
      dataType: 'json',
      data: formData,
      success: function(res){
        console.log(res);
          offOverlay();
          if(res.val)
            location.reload();
          else
            alert(res.msg);
      }
  }).fail(function(jqXHR,status,errorThrown) {
      console.log("Error : "+errorThrown);
  });
}