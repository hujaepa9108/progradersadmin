//fetch data to populate instructor list
$(document).ready(function() {
  var url=window.location.href.split('/');
  var type=url[url.length-1];
  var datatable=$('#uni_list').DataTable({  
       processing:true,  
       serverSide:true,
       order:[],  
       ajax:{  
            url:$("#url").val()+"instructor/uniList/"+type,  
            type:"post"  
       },
       language: {
        searchPlaceholder: "Enter uni name"
       },
       ordering: false,
       columns: [
          {render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
          }},
          null,
          null,
          null
      ]
  });//end of data table
});//end of document ready



