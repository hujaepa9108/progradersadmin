
$(document).ready(function() {
    //OVERLAY STUFF
    function setOverlay(){
        $("#overlay").css({
            display : "block"
        });
    }

    function offOverlay() {
        setTimeout(function() {
            $("#overlay").css({
                display : "none"
            });
        },3000);
    }
    //END OF OVERLAY STUFF//
    
    //FORM VALIDATION//
    $("#add_section").validate({
        //ERROR HANDLING//
        ignore: [], //enable validation on hidden field
        highlight: function(element) {
            $(element).closest('.form-control').addClass('is-invalid');
        },

        unhighlight: function(element) {
            $(element).closest('.form-control').removeClass('is-invalid');
        },

        errorElement: 'span',

        errorClass: 'badge badge-danger',
        
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) 
                error.insertAfter(element.parent());
            else 
                error.insertAfter(element);
        },
        //END OF ERROR HANDLING//
         
        rules:{
            section:{
                required: true
                // remote:  $("#url").val()+"section/checkExist"// to check exist code
            }
        },
        // messages: {
        //     section:{
        //         remote: "This section has been added!"//set message
        //     }
        // },
        submitHandler: function(form,event) {
            event.preventDefault();
            var url=window.location.href.split("/");
            var courseId=url[url.length-1];
            var uniId=url[url.length-2];
            var url = $("#url").val() + "section/processAdd";//url setup
            var data = $(form).serialize()+"&courseId="+courseId;//form serialize for jquery validation
            setOverlay();
            $.ajax({
                url: url,
                type: 'post',
                beforeSend: function() {
                    setOverlay();
                }, 
                dataType: 'json',
                data: data,
                success: function(res){
                    offOverlay();
                    window.location.href=$("#url").val()+"section/list/"+uniId+"/"+courseId;
                }
            }).fail(function(jqXHR,status,errorThrown) {
                console.log("Error : "+errorThrown);
            });
        }
    });
    //END OF FORM VALIDATION//
    
});

//ajaxing the course data
var vuejs = new Vue({
    el:"#inst",
    data: function() {
        return {
            course: [],
            message:"",
            total:0,
            selected:''
        }
    },
    mounted: function() {       
        var url = $("#url").val()+"/course/getCourse";
        axios
            .get(url)
            .then(response => {
                this.total=response.data.course.length;
                this.course=response.data.course;
                if(this.total==0)
                    this.message="Please add new course first before proceed to register the lecturer!";
            });
    },
    methods:{
        onchange: function(){
           if(this.selected==="addcourse")
               window.location.href=$("#url").val()+"course/add";
            //redirect to add course page
        }
    }

});