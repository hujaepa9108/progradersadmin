var url=window.location.href.split("/");
const courseId=url[url.length-2];//get course id
const uniId=url[url.length-3];//get uni id
const sectionId=url[url.length-1];//get section id 
const instId=$("#instId").val();

function setOverlay(){
    $("#overlay").css({
        display : "block"
    });
}

function offOverlay() {
    setTimeout(function() {
        $("#overlay").css({
            display : "none"
        });
    },3000);
}

//ASSIGNING INSTRUCTOR
$('.assignInst').click(function(){
var table= $("#inst_list tbody");
  $.ajax({
    url: $("#url").val()+"section/getMembersInst/"+courseId,
    type: 'get',
    success: function(res){
        var html="";
        var data=JSON.parse(res);
        $.each(data, function(idx, elem){
          html+="<tr><td>"+(++idx)+"</td>";
          html+="<td>"+elem.name+"</td>";
          html+="<td><a href='"+$("#url").val()+"section/assignInst/"+btoa(elem.inst_id)+"/"+sectionId+"/"+courseId+"' class='btn btn-success btn-sm assign'>Assign</a></td></tr>";
          // html+="<td><button' class='btn btn-success btn-sm assign'>Assign</button></td></tr>";
        });
        table.html(html);
        $('#inst_modal').on('show.bs.modal', function() {
          $(".assign").click(function(e){
            e.preventDefault();
            var urlRedirect = $(this).attr('href');

            $.ajax({
              url:urlRedirect,
              type:"get",
              success:function(res){
                offOverlay();
                var resParse=JSON.parse(res);
                if(resParse.val)
                  location.reload();
                else
                  alert(resParse.msg);
              }
            }).fail(function(jqXHR,status,errorThrown) {
                console.log("Error : "+errorThrown);
            });

          });
        }).modal('show'); 
    }
  }).fail(function(jqXHR,status,errorThrown) {
        console.log("Error : "+errorThrown);
  });
});


//UPDATE INSTRUCTOR
$('.changeInst').click(function(){
var table= $("#inst_list tbody");
  $.ajax({
    url: $("#url").val()+"section/getMembersInst/"+courseId+"/"+instId,
    type: 'get',
    success: function(res){
        var html="";
        var data=JSON.parse(res);
        $.each(data, function(idx, elem){
          html+="<tr><td>"+(++idx)+"</td>";
          html+="<td>"+elem.name+"</td>";
          html+="<td><a href='"+$("#url").val()+"section/changeInst/"+btoa(elem.inst_id)+"/"+sectionId+"' class='btn btn-warning btn-sm select'>Select</a></td></tr>";
        });
        table.html(html);
        $('#inst_modal').on('show.bs.modal', function() {
          $(".select").click(function(e){
            e.preventDefault();
            var urlRedirect = $(this).attr('href');

            $.ajax({
              url:urlRedirect,
              type:"get",
              success:function(res){
                offOverlay();
                var resParse=JSON.parse(res);
                if(resParse.val)
                  location.reload();
                else
                  alert(resParse.msg);
              }
            }).fail(function(jqXHR,status,errorThrown) {
                console.log("Error : "+errorThrown);
            });

          });
        }).modal('show'); 
    }
  }).fail(function(jqXHR,status,errorThrown) {
        console.log("Error : "+errorThrown);
  });
});

//ASSIGNING STUDENT
$('.assignStud').click(function(e){
  e.preventDefault();
  var table= $("#stud_list tbody");
  $.ajax({
    url: $("#url").val()+"section/getNonSectStud/"+courseId+"/"+sectionId,
    type: 'get',
    success: function(res){
        var html="";
        console.log(res);
        var data=JSON.parse(res);
        console.log(res);
        if(data.length>0) {
          $.each(data, function(idx, elem){
            html+="<tr><td>"+(++idx)+"</td>";
            html+="<td>"+elem.name+"</td>";
            html+="<td>"+elem.metric_no+"</td>";
            html+="<td><input type='checkbox' name='student[]' value='"+btoa(elem.user_id)+"'></td></tr>";
            // html+="<td><button' class='btn btn-success btn-sm assign'>Assign</button></td></tr>";
          });
        }

        else
          html+="<tr><td colspan='4' align='center'><b>0 added students</b></td>";

        table.html(html);
        $('#stud_modal').on('show.bs.modal', function() {
          e.preventDefault();

          //SET THE ROW HIGHLIGHTS
          $("input[type='checkbox']").change(function (e) {
              if ($(this).is(":checked")) {
                  $(this).closest('tr').addClass("bg-warning");
              }
              else 
                  $(this).closest('tr').removeClass("bg-warning");
          });

          //click assigned stud
          $(".addStud").click(function(e){
              e.preventDefault();
              if($("input[type='checkbox']").is(":checked")){ 

                var id = [];
                $("input[type='checkbox']:checked").each(function(i){
                  id[i]= $(this).val();
                });

                var dataForm ={id,sectionId};
                //PROCESSING ADD NEW STUDENTS IN SECTION
                $.ajax({
                  url:$("#url").val()+"section/addStud",
                  type:"post",
                  data:dataForm,
                  success:function(res){
                    if(res){
                      location.reload();
                    }
                    else{
                      bootbox.alert("Server-side Error : ");
                    }
                  }
                }).fail(function(jqXHR,status,errorThrown) {
                  bootbox.alert("Server-side Error : "+errorThrown);
                });
              }
            });

        }).modal('show');
        }//end of success outer 
    }).fail(function(jqXHR,status,errorThrown) {
        bootbox.alert("Server Error : "+errorThrown);
  });//end of ajax outer
});//end of click event

//LIST OF STUDENTS
window.onload=function() {
  $.ajax({
    url: $("#url").val()+"section/getMembersStud/"+"/"+courseId+"/"+sectionId,
    type: 'get',
    contentType: "application/json",
    success: function(res){
        var html="";
        var studentTab= $("#student_list tbody");
          if(res.length>0){

          var data=JSON.parse(res);
            $.each(data, function(idx, elem){
              html+="<tr><td>"+(++idx)+"</td>";
              html+="<td>"+elem.name+"</td>";
              html+="<td>"+elem.email+"</td>";
              html+="<td>"+elem.metric_no+"</td>";
              html+="<td>"+elem.created_at+"</td>";
              // html+="<td><a href='"+$("#url").val()+"section/removeStudent/"+btoa(elem.id)+"/"+sectionId+"' class='btn btn-danger btn-sm select'>Remove</a></td></tr>";
              html+="<td><button class='btn btn-danger' onclick='remove(\""+btoa(elem.user_id)+"\",\""+btoa(elem.section_id)+"\")'><i class='fas fa-trash'></i></td></tr>";
            });
          }
        else{
          html+="<tr><td colspan='6' align='center'><b>0 added students</b></td>";
        }

        studentTab.html(html);
      }
  }).fail(function(jqXHR,status,errorThrown) {
    bootbox.alert("Server-side Error : "+errorThrown);
  });
}


function remove(studentId,sectionId){
    bootbox.confirm({ 
      size: "medium",
      message: "<span class='bootbox-custom-format'>Are you sure you want to <span class='bootbox-red-text'>remove</span> this student ? </span>",
      callback: function(result){
        if(result)
          removeAjax(studentId,sectionId);
      }
    });
  }

  function removeAjax(studentId,sectionId){
    var formData =  {
      studentId,sectionId
    };
    var url = $("#url").val() + "section/processRemoveStud/"+studentId+"/"+sectionId;//url setup
    $.ajax({
        url: url,
        type: 'post',
        beforeSend: function() {
            setOverlay();
        }, 
        dataType: 'json',
        data: formData,
        success: function(res){
          console.log(res);
            offOverlay();
            if(res.val)
              location.reload();
            else
              alert(res.msg);
        }
    }).fail(function(jqXHR,status,errorThrown) {
        console.log("Error : "+errorThrown);
    });
  }