//OVERLAY STUFF
function setOverlay(){
    $("#overlay").css({
        display : "block"
    });
}

function offOverlay() {
    setTimeout(function() {
        $("#overlay").css({
            display : "none"
        });
    },3000);
}

//ajaxing the course and section data
// var vuejs = new Vue({
//     el:"#inst",
//     data: {
//         course: [],
//         section:[],
//         selected:'',
//         isDisabled:true
//     },
//     mounted: function() {       
//         var url = $("#url").val()+"course/getCourse";
//         axios
//             .get(url)
//             .then(response => {
//                 if(response.data.course.length>0) {//if courses ad
//                     this.course=response.data.course;
//                 }
//             });
//     },
//     methods:{
//         onchange(event){
//             this.selected=event.target.value;
//             if(this.selected!=""){
//                 var url = $("#url").val()+"section/getSection/"+this.selected;
//                 axios
//                     .get(url)
//                     .then(response => {
//                         if(response.data.section.length>0) {
//                             this.section=response.data.section;
//                             this.isDisabled=false;
//                         }
//                 });
//             }//end if
            
//             else{
//                 this.isDisabled=true;
//             }
//         }//end of onchange
//     }
// });



//FORM VALIDATION//
$(document).ready(function(){
    $("#add_student").validate({
    //ERROR HANDLING//
    ignore: [], //enable validation on hidden field
    highlight: function(element) {
        $(element).closest('.form-control').addClass('is-invalid');
    },

    unhighlight: function(element) {
        $(element).closest('.form-control').removeClass('is-invalid');
    },

    errorElement: 'span',

    errorClass: 'badge badge-danger',
    
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) 
            error.insertAfter(element.parent());
        else 
            error.insertAfter(element);
    },
    //END OF ERROR HANDLING//
    
    //CUSTOM RULES
    rules:{
        email:{
            email: true,
            required: true,
            remote:  $("#url").val()+"student/checkExist"// to check exist instructor
        },
        name:{
            required: true
        },
        idno:{
            required: true
        },
    },
    messages: {
        email:{
            email: "Invalid format of email",
            remote: "This student has been added!"//set message
        }
    },
    // END OF CUSTOM RULES
    
    submitHandler: function(form,event) {
        event.preventDefault();
        
        var url = $("#url").val() + "student/processAdd";//url setup
        var data = $(form).serialize();//form serialize for jquery validation
        setOverlay();
        $.ajax({
            url: url,
            type: 'post',
            beforeSend: function() {
                setOverlay();
            }, 
            dataType: 'json',
            data: data,
            success: function(res){
                offOverlay();
                if(res.val){
                    var url=window.location.href.split("/");
                    var uniId=url[url.length-1];
                    window.location.href=$("#url").val()+"student/list/"+uniId;
                }
                else
                    alert("Technical error with the server");
            }
        }).fail(function(jqXHR,status,errorThrown) {
            console.log("Error : "+errorThrown);
        });
    }
    });
});

//GET URL PARAMETER VALUE
var url=window.location.href.split("/");
var getLastParam=url[url.length-1];
$("#uniId").val(getLastParam);

new Vue({
    el:"#course_list",
    data: {
        course: []
    },
    mounted: function() {
        var url=window.location.href.split("/");
        var uniId=url[url.length-1];       
        var url = $("#url").val()+"instructor/getCourse/"+uniId;
        axios
            .get(url)
            .then(response => {
                if(response.data.course.length>0) {//if courses ad
                    this.course=response.data.course;
                }
            });
    }
});